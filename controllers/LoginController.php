<?php
namespace app\controllers;

use app\models\MyDebug;
use Yii;
use yii\web\Controller;
use app\models\LoginForm;
use app\models\Teach;
use app\models\PublicCity;
use yii\web\Session;
use yii\web\Cookie;
use yii\helpers\Url;

class LoginController extends Controller
{

    public $layout = 'main_login.php';

    public function init()
    {
        $session = new Session;
        $session->open();
        if($session->has('id_teach'))
        {
            header("Location:".Url::toRoute('/portfolio'));
            exit();
        }
    }

    public function actionIndex()
    {

        $model = new LoginForm;
        $session = new Session;
        $session->open();

        if ($model->load(Yii::$app->request->post()) && $model->validate()) {
            $request = Yii::$app->request->post();
            $username = $request['LoginForm']['username'];
            $password = $request['LoginForm']['password'];
            $session->set('username',$username);
            //MyDebug::P(2,1);
            if($request['LoginForm']['list_city'][0] ==='city_select')
            {
                Yii::$app->session->setFlash('not_city');
                return $this->redirect(['/login/']);
            }

            $session['city_id'] = $request['LoginForm']['list_city'][0];

//            MyDebug::P([
//                $request['LoginForm']['list_city'][0]
//            ],1);

//            $_COOKIE['city_id'] = $session['city_id'];

//            $hh = new Cookie();
//            $hh->name = 'city_id';
//            $hh->value = $session['city_id'];
//            \Yii::$app->request->cookies->add($hh);

            if(Teach::login($username, $password)) {
                return $this->redirect(['/portfolio/index/']);
            } else {
                Yii::$app->session->setFlash('error_login');
                return $this->redirect(['/login/']);
            }
        }

        return $this->render('index', array('model' => $model, 'city_select' => $model->list_city));
    }

    public function actionRestore()
    {
        $session = new Session;
        $session->open();
        $request = \Yii::$app->request->post();
        $email = $request['restore_email'];
        $password = trim($request['restore_password']);
        $city = trim($request['list_city']);
        if($city ==='city_select') {
            \Yii::$app->session->setFlash('not_city');
            return $this->redirect(['/login/restoreindex']);
        }

        $session['city_id'] = $city;
        $teach = Teach::getTeachEmail($email);

        if($teach)
        {

            $email = !empty($teach['corp_email_teach']) ? $teach['corp_email_teach'] : $teach['email_teach'] ;
            $subject = "Восстановление пароля Logbook.itstep.org";

            $encrypt_id_teach = urlencode( \Yii::$app->encrypter->encrypt($teach['id_teach']) ) ;
            $encrypt_password = urlencode( \Yii::$app->encrypter->encrypt($password) ) ;

            $url =\Yii::$app->urlManager->createAbsoluteUrl(['login/recoverpassword', 'i' =>"$encrypt_id_teach", 's' =>"$encrypt_password", ]);
            $html = "Ваш новый пароль для входа в LOGBOOK: ".$password."<br>  Для подтверждения смены пароля перейдите по ссылке: ".
             "<a href=".$url.">!!! ПОДТВЕРДИТЬ СМЕНУ ПАРОЛЯ !!!</a>";

            $postData = [
                'from' => 'logbook@itstep.org',
                'email' => $email,
                'subject' => $subject,
                'body' => $html
            ];
            \Yii::$app->send->sendIt($postData);

            \Yii::$app->session->setFlash('restore_pass');
            return $this->redirect(['/login/']);
        }
        else{
            \Yii::$app->session->setFlash('error_email');
            return $this->redirect(['/login/restoreindex']);
        }
    }
    public function actionRecoverpassword()
    {
        $encrypt_id_teach = urldecode(\Yii::$app->request->get('i')) ;
        $encrypt_password= urldecode(\Yii::$app->request->get('s')) ;

        $decrypt_id_teach = \Yii::$app->encrypter->decrypt($encrypt_id_teach);
        $decrypt_password = \Yii::$app->encrypter->decrypt($encrypt_password);

        $res =  Teach::RecoverPassword($decrypt_id_teach,$decrypt_password);

        if($res){
            \Yii::$app->session->setFlash('restore_pass_ok');
            return $this->redirect(['/login/']);
        }
        else{
            return $this->redirect(['/login/']);
        }
    }

    public function actionRestoreindex()
    {
        $model= new LoginForm;
        return $this->render('restore', ['city_select'=>$model->list_city]);
    }
    public function beforeAction($action)
    {
        if (Yii::$app->session->has('lang')) {
            Yii::$app->language = Yii::$app->session->get('lang');
        } else {
            Yii::$app->language = 'ru-RU';
            Yii::$app->session->set('lang', 'ru-RU');
        }

        return parent::beforeAction($action);
    }

    public function actionError(){
        echo 1;
        exit;
    }
}
