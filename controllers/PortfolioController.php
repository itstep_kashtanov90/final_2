<?php
/**
 * Created by PhpStorm.
 * User: kashtanov
 * Date: 05.04.2016
 * Time: 10:59
 */

namespace app\controllers;

use app\models\BaseModel;
use app\models\MyDebug;
use app\models\Stud;
use app\models\Tgroups;
use app\models\UploadedFileModel;
use yii\filters\VerbFilter;
use app\models\DigestModel;
use app\models\DigestMaterials;
use yii\helpers\ArrayHelper;
use yii\web\UploadedFile;
use yii\helpers\Url;
use yii\web\Controller;

use app\models\ImageModel;

class PortfolioController extends BaseController
{

//    public function behaviors()
//    {
//        return [
//            'verbs' => [
//                //'class' => VerbFilter::className(),
//                'actions' => [
//                    //'new'               => ['post'],
//                    'replace-file'      => ['post'],
//                    'delete-material'   => ['post'],
//                    'details'           => ['get'],
//                ],
//            ],
//        ];
//    }

    public function actionIndex()
    {
        $model = new DigestModel();
        //echo 1; exit;
        //MyDebug::P($model->getAllPortfolio(),1);
        return $this->render('index', ["data" => $model->getAllPortfolio()]);
    }

    public function actionLoad()
    {

        $data = [
            "model"         => new DigestModel(),
            "file_model"    => new ImageModel(),
            "all_marks"     => ['' => 'Оценка', 0, 1, 2, 3, 4, 5, 6, 7, 8 ,9, 10, 11, 12],
            "all_teach"     => ArrayHelper::merge(['' => \Yii::t('js_superusser', 'sel_teach')], parent::actionGetAllTeach(true)),
            "type_work"     => [
                ''          => "Тип работы",
                'video'     => "Видео",
                'image'     => "Изображение",
                'file'      => "Файл",
                'pdf'       => "Презентация",
            ],
        ];

        return $this->render('load', $data);
    }

    public function actionView()
    {
        return $this->render('view');
    }

    public function actionDetails($id_digest)
    {
        $stud = new Stud();
        $tgroups = new Tgroups();
        $digest = new DigestModel();
        $materials = new DigestMaterials();

        $data['digest'] = $digest
            ->find()
            ->where(['=', 'id_digest', $id_digest])
            ->all()[0];

        $data['stud'] = $stud
            ->find()
            ->select(["fio_stud"])
            ->where(['=', 'id_stud', $data['digest']->id_stud])
            ->all()[0];

        $data['tgroups'] = $tgroups
            ->find()
            ->select(["name_tgroups"])
            ->where(['=', 'id_tgroups', $data['digest']->id_tgroups])
            ->all()[0];

        $data['materials'] = $materials
            ->find()
            ->where(['=', 'id_digest', $id_digest])
            ->all();

        $prepare_material = [];

        foreach($data['materials'] as $model){
            $prepare_material[$model->id_digest_materials] = $this->prepare_material(
                $model->type,
                $model->path,
                $stud->id_stud == '' ? $data['digest']->id_stud : $stud->id_stud,
                $model->file_name,
                $model->id_digest_materials);
        }

        $data['ReplaceImage'] = new ImageModel();
        $data['PrepareMaterials'] = $prepare_material;

        return $this->render('details', $data);
    }

    public function actionNew()
    {
        $post_data = \Yii::$app->request->post('DigestModel');

        $model = new DigestModel();

        $date = new \DateTime();

        if(isset($post_data['path']) && !empty($post_data['path'])){
            $path = $post_data['path'];
        } else {
            list($file_name, $path) = $this->SaveAndGetNameFile($post_data['id_stud'], 'image');
        }

        $post_data['type'] = 'image';
        $post_data['path'] = $path;
        $post_data['file_name'] = $file_name;
        //$post_data['date'] = $date->format("Y-m-d H:m:i");

        $data['DigestModel'] = $post_data;

        if($model->load($data) && $model->validate()){
            $model->save();
        } else {
            MyDebug::P(['error loading...', 'error' => '561238'], 1);
        }

        return \Yii::$app->response->redirect(Url::to(['details', 'id_digest' => $model->id_digest]));
    }

    public function actionNewMaterials()
    {

        $digest = new DigestModel();
        $materials = new DigestMaterials();

        $post_data = \Yii::$app->request->post();
        $post_data['id_stud'] = $digest
            ->find()
            ->select(["id_stud"])
            ->where(['=', 'id_digest', $post_data['id_digest']])
            ->scalar();

        $date = new \DateTime();

        if(isset($post_data['DigestMaterials']['path']) && !empty($post_data['DigestMaterials']['path'])){
            $path = $post_data['DigestMaterials']['path'];
        } else {
            list($file_name, $path) = $this->SaveAndGetNameFile($post_data['id_stud']);
        }

        $data['DigestMaterials'] = [
            'id_digest'     => $post_data['id_digest'],
            'id_teach'      => $_SESSION['id_teach'],
            'id_stud'       => $post_data['id_stud'],
            'comment'       => $post_data['comment'],
            'file_name'     => $file_name,
            'path'          => $path,
            'type'          => $post_data['type'],
            //'date'          => $date->format("Y-m-d H:m:i"),
        ];

        if($materials->load($data) && $materials->validate()){
            $materials->save();
        }

        //MyDebug::P('пепец', 1);

        return \Yii::$app->response->redirect(Url::to(['details', 'id_digest' => $post_data['id_digest']]));
    }

    private function SaveAndGetNameFile($id_stud, $type = 'file'){

        if($type == 'file') {
            $UploadedFile = new UploadedFileModel();
        } elseif($type == 'image'){
            $UploadedFile = new ImageModel();
        }

        //MyDebug::P('начало');

        $UploadedFile->file = UploadedFile::getInstance($UploadedFile, 'file');

        $file_name = $UploadedFile->file->baseName . '.' . $UploadedFile->file->extension;

        $path = $UploadedFile->GetPath() . $id_stud;

        $new_file_name = UploadedFileModel::getRandomFileName($path, $UploadedFile->file->extension);

        $PathForSave = $path . "/" . $new_file_name;

        //MyDebug::P('начало записи');

        //MyDebug::P(['1' => $UploadedFile->validate()], 1);

        if ($UploadedFile->file && $UploadedFile->validate()) {
            //MyDebug::P('запись', 1);
            if(!file_exists($path)){
                mkdir($path, 0777, true);
            }
            $UploadedFile->file->saveAs($PathForSave, false);
        }

        //MyDebug::P('dfgfdgfdgdfgdfg',1);

        return [$file_name, $new_file_name];
    }

    public function actionReplaceFile(){
        $digest = new DigestModel();
        $UploadedFile = new UploadedFileModel();
        $UploadedFile = new ImageModel();

        $post_data = \Yii::$app->request->post();

        $this_digest = $digest
            ->find()
            ->where(['=', 'id_digest', $post_data['id_digest']])
            ->all()[0];

        if(isset($post_data['path'])){
            $path = $post_data['path'];
        } else {
            list($file_name, $path) = $this->SaveAndGetNameFile($this_digest->id_stud, 'image');
        }

        $old_path = $UploadedFile->GetPath() . $this_digest->id_stud . "/" . $this_digest->path;
        if(file_exists($old_path)){
            unlink($old_path);
        }

        $this_digest->type = 'image';
        $this_digest->path = $path;
        $this_digest->file_name = $file_name;
        $this_digest->update(true);

        return \Yii::$app->response->redirect(Url::to(['details', 'id_digest' => $post_data['id_digest']]));
    }

    public function actionDeleteDigest(){

        $digest = new DigestModel();
        $materials = new DigestMaterials();
        $UploadedFile = new UploadedFileModel();

        $post_data = \Yii::$app->request->post();

        $this_digest = $digest
            ->find()
            ->where(['=', 'id_digest', $post_data['id_digest']])
            ->all()[0];

        $this_materials = $materials
            ->find()
            ->where(['=', 'id_digest', $post_data['id_digest']])
            ->all();

        $old_path = $UploadedFile->GetPath() . $this_digest->id_stud . "/" . $this_digest->path;
        if(file_exists($old_path)){
            unlink($old_path);
        }

        if(count($this_materials)){
            foreach($this_materials as $val){
                $old_path = $UploadedFile->GetPath() . $this_digest->id_stud . "/" . $val->path;
                if(file_exists($old_path)){
                    unlink($old_path);
                }
            }
        }

        $this_digest->delete();
        return \Yii::$app->response->redirect(Url::to(['index']));
    }

    public function actionDeleteMaterial(){

        if(\Yii::$app->request->isAjax) {

            $digest = new DigestModel();
            $materials = new DigestMaterials();
            $UploadedFile = new UploadedFileModel();

            $post_data = \Yii::$app->request->post();

            $this_digest = $digest
                ->find()
                ->where(['=', 'id_digest', $post_data['id_digest']])
                ->all()[0];

            $this_material = $materials
                ->find()
                ->where(['=', 'id_digest', $post_data['id_digest']])
                ->andWhere(['=', 'id_digest_materials', $post_data['id_material']])
                ->one();

            if (count($this_material)) {
                $old_path = $UploadedFile->GetPath() . $this_digest->id_stud . "/" . $this_material->path;
                if(file_exists($old_path)){
                    unlink($old_path);
                }
            }

            $this_material->delete();
        }
    }

    public function actionEditDigest(){
        if (\Yii::$app->request->isAjax) {
            $digest = new DigestModel();

            $id_digest = \Yii::$app->request->post('id_digest');
            $elem = \Yii::$app->request->post('elem');
            $val = \Yii::$app->request->post('val');

            $data['digest'] = $digest
                ->find()
                ->where(['=', 'id_digest', $id_digest])
                ->all()[0];
            $page = 'ne-cho';
            switch ($elem) {
                case "name":
                    $page = $data['digest']->name . " / - / " . $val;
                    $data['digest']->name = $val;
                    break;
                case "mark":
                    $page = $data['digest']->mark . " / - / " . $val;
                    $data['digest']->mark = $val;
                    break;
                case "comment":
                    $page = $data['digest']->comment . " / - / " . $val;
                    $data['digest']->comment = $val;
                    break;
            }

            $data['digest']->update(true);
            return $page;
        } else {
            return "error - 30012";
        }
    }

    public function actionEditMaterial(){
        if (\Yii::$app->request->isAjax) {

            $id_material = \Yii::$app->request->post('id_material');
            $comment = \Yii::$app->request->post('comment');

            $ThisMaterial = DigestMaterials::find()
                ->where(['=', 'id_digest_materials', $id_material])
                ->one();

            $old_comment = $ThisMaterial->comment;

            $ThisMaterial->comment = $comment;

            if($ThisMaterial->validate()){
                $ThisMaterial->update(true);
            }
            return $old_comment . " / => / " . $comment;
        } else {
            return "error - 30015";
        }
    }

    public function actionDownloadFile($id_material){


        $UploadedFile = new UploadedFileModel();

        $data = DigestMaterials::find()->where(['=', 'id_digest_materials', $id_material])->one();

        //внешняя или внутренная ссылка ( если нет названия файла )
        if (empty($data->file_name)) {

//            $name = explode("/", $data->path);
//            $file_name = $name[ count($name) - 1 ];
//
//            $file_path = $data->path;

        } else {

            $prefix = BaseModel::getPrefix();

            $data_Digest = DigestModel::find()->where(['=', 'id_digest', $data->id_digest])->one();

            $file = "";

            if (!isset($_SERVER['WINDIR'])) {
                $file .= "/var/www/final.itstep.org/web/";
                //$file .= $_SERVER['DOCUMENT_ROOT'] . '/';
            }

            $file .= "load_portfolio/";
            $file .= "$prefix/";
            $file .= $data->id_stud == '' ? $data_Digest->id_stud : $data->id_stud . "/";
            $file .= $data->path;

            $file_path = $file;

            $file_name = $data->file_name;

            if(count($data) >= 1) {
                return \Yii::$app->response->sendFile($file_path, $file_name);
            }
        }
    }

    private function prepare_material($type, $path, $id_stud, $file_name, $id_digest_materials = null){

        if($type == 'image') {

            //если внешняя ссылка ( т.е. если нет названия файла )
            if (empty($file_name)) {
                $prepared_path = $path;
            } else {
                $prepared_path = DigestModel::GetPathCity() . $id_stud . "/" . $path;
            }

        }

        if($type == 'video') {

            $prepared_path = $path;

            //https://youtu.be/47kltPROcUQ
            //https://www.youtube.com/embed/47kltPROcUQ

            //https://www.youtube.com/watch?v=FNPhHLloQhg

            //если ссылка на ютуб не та, то делает ту что надо
            $etx = explode("/", $path);
            foreach($etx as $val){
                if($val == 'youtu.be'){
                    $prepared_path = 'https://www.youtube.com/embed/' . $etx[ count($etx) - 1 ];
                }
            }

//$subject = $path;

//            $patterns = [
//
//            ];
//

//            $pattern = '/youtu.be/';
//            preg_match($pattern, $subject, $matches);

//            $pattern = '/watch?v=(.*?)/';
//            preg_match_all($pattern, $subject, $matches);
            //var_dump($matches{1});



//            $pattern = '[^\s]*q(?v=)[^\s]*';
//            $matches = preg_replace('[^\s]*q(?!werty)[^\s]*', $subject);

//            $r = "Агджаяyoutu.be/целомудренная, и хорошая
//Агджи - целомудренная, и хорошая
//https://youtu.be/47kltPROcUQ
//Агнес - целомудренная, святая
//Агнус - целомудренная, святая
//Ада - благородный вид и благородная
//Адайн - огонь ";
//
//            $subject = $r;
//
//            preg_match_all('/ (youtu.be) \/(.*?) /', $subject, $matches);
//
//
//
//            MyDebug::P([$path, $matches]);

        }

        if($type == 'pdf') {

            if (empty($file_name)) {
                $type = 'file';
            } else {
                $prepared_path = DigestModel::GetPathCity() . $id_stud . "/" . $path;

                $etx = explode(".", $path);
                $etx = $etx[count($etx) - 1];
                if ($etx != 'pdf') {
                    $type = 'file';
                }
            }
        }

        if($type == 'file'){

            $prepared_path = $id_digest_materials;

            //если внешняя ссылка ( т.е. если нет названия файла )
            if (empty($file_name)) {

                $prepared_path = $path;
                $file_name = 'Скачать';
                $type = 'file_outside';

            }

        }

        $expansion = explode(".", $file_name);

        return $this->renderPartial($type, [
            'prepared_path'     => $prepared_path,
            'file_name'         => $file_name,
            'expansion'         => $expansion[ count($expansion) - 1 ],
        ]);
    }
}