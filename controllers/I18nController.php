<?php

    namespace app\controllers;

    use yii;
    use yii\web\Controller;

    class I18nController extends Controller
    {

        public function actionRu()
        {
            \Yii::$app->session->set('lang', 'ru-RU');

            return $this->redirect(Yii::$app->request->referrer);
        }


        public function actionEn()
        {
            \Yii::$app->session->set('lang', 'en-US');

            return $this->redirect(Yii::$app->request->referrer);
        }

        public function actionBg()
        {
            \Yii::$app->session->set('lang', 'bg-BG');

            return $this->redirect(Yii::$app->request->referrer);
        }

        public function actionPt()
        {
            \Yii::$app->session->set('lang', 'pt-PT');

            return $this->redirect(Yii::$app->request->referrer);
        }

        public function actionLt()
        {
            \Yii::$app->session->set('lang', 'lt-LT');

            return $this->redirect(Yii::$app->request->referrer);
        }

        public function actionUa()
        {
            \Yii::$app->session->set('lang', 'ua-UA');

            return $this->redirect(Yii::$app->request->referrer);
        }


    }
