<?php
    namespace app\controllers;

    use app\models\DigestModel;
    use app\models\Tgroups;
    use Yii;
    use yii\helpers\Html;
    use yii\web\Controller;
    use yii\web\Request;
    use yii\web\Response;
    use yii\web\Session;
    use yii\helpers\Url;
    use app\models\Teach;
    use yii\helpers\Json;
    use yii\helpers\ArrayHelper;
    use app\models\BaseModel;
    use app\models\UploadedFileModel;
    use app\models\ImageModel;
    use app\models\DigestMaterials;

    class BaseController extends Controller
    {
        /**
         * function for do not write every time Yii:app->request->post('')
         * @param $param
         * @return array|mixed
         */

    protected function c_post($param){
        return Yii::$app->request->post($param);
    }

    public function init()
    {
        $session = new Session;
        $session->open();

        if ( is_null($this->c_post('curl')) ) {
            if (!$session->has('id_teach')) {
                header("Location:" . Url::toRoute('/login'));
                //return \Yii::$app->response->redirect(Url::toRoute('/login'));
                exit();
            }
        }
    }

    public function actionGetCityPrefix()
    {
        return BaseModel::getPrefix();
    }

    public function actionGetAllTeach($return = false) {
        $session = new Session;
        $session->open();
        $role = $session->get('role');
        if ($role != 3) {
            return false;
        }

        $model = new Teach();
        $all_teach = $model->find()->where(['arc_teach' => 0])->orderBy('fio_teach')->asArray()->all();
        $i=0;
        $res = array();
        foreach($all_teach as $value) {
            $res[$value['id_teach']] = $value['fio_teach'];
            //$res[$i]['id_teach'] = $value['id_teach'];
            $i++;
        }
        if($return){
            return $res;
        } else {
            echo Json::encode($res);
        }
    }

    public function actionForType(){
        if(\Yii::$app->request->isAjax) {

            $view = Yii::$app->request->post('type');

            $data = [
                'model' => [
                    'file'      => new UploadedFileModel(),
                    'digest'    => new DigestMaterials(),
                    'image'     => new ImageModel(),
                ],
            ];

            return $this->renderPartial($view, $data);
        }
    }

    public function actionGetGroupsByTeach(){
        if(\Yii::$app->request->isAjax) {
            $group = new Tgroups();
            $id_teach = Yii::$app->request->post('id_teach');
            $groups = $group->getActGroups($id_teach);
            echo Json::encode($groups);
        }
    }

    public function actionGetStudByGroup(){
        if(\Yii::$app->request->isAjax) {
            $group = new DigestModel();
            $id_tgroups = Yii::$app->request->post('id_tgroups');
            $students = $group->getStudByGroups($id_tgroups);
            echo Json::encode($students);
        }
    }

    public function actionLogout()
    {
        $session = new Session;
        $session->destroy();
        Yii::$app->response->redirect(array('/'));
    }


    public function actionChangePassword()
    {
        $session = new Session;
        $session->open();
        $id_teach = $session->get('id_teach');
        $request = Yii::$app->request;
        $pass = $request->post('pass');
        $pass_repeat = $request->post('pass_repeat');
        $model = new Teach();
        $result = $model->changePassword($id_teach,$pass,$pass_repeat);
        if($result == 1)
            echo json_encode(array('msg'=>1));
        elseif($result =='not_empty_pass')
            echo json_encode(array('msg'=>$result));
        elseif($result =='pass_not_match')
            echo json_encode(array('msg'=>$result));
    }


    public function beforeAction($action)
    {
        if (Yii::$app->session->has('lang')) {
            Yii::$app->language = Yii::$app->session->get('lang');
        } else {
            Yii::$app->language = 'ru-RU';
            Yii::$app->session->set('lang', 'ru-RU');
        }

        return parent::beforeAction($action);
    }

    public function actionGetLang(){
        $lang  = Yii::$app->session->get('lang');
        Yii::$app->response->format = Response::FORMAT_JSON;
        return Json::encode(['lang' => $lang]);
    }
}