<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "stud".
 *
 * @property string $id_stud
 * @property string $id_tgroups
 * @property string $fio_stud
 * @property string $adr_stud
 * @property string $phone_stud
 * @property string $birthdat_stud
 * @property string $spec_stud
 * @property integer $learnstop_stud
 * @property integer $dateback_stud
 * @property string $comment_stud
 * @property resource $img_stud
 * @property integer $price
 * @property string $form_pay
 * @property string $id_1c
 * @property string $login
 * @property string $passwd
 * @property integer $free_vizit
 * @property string $photo_pas
 * @property integer $user_last_date
 * @property string $last_date_vizit
 * @property string $access_token
 */
class Stud extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'stud';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_tgroups', 'learnstop_stud', 'dateback_stud', 'price', 'free_vizit', 'user_last_date'], 'integer'],
            [['birthdat_stud', 'last_date_vizit'], 'safe'],
            [['spec_stud', 'img_stud', 'photo_pas'], 'string'],
            [['id_1c'], 'required'],
            [['fio_stud'], 'string', 'max' => 50],
            [['adr_stud', 'form_pay'], 'string', 'max' => 100],
            [['phone_stud', 'comment_stud'], 'string', 'max' => 255],
            [['id_1c', 'login', 'passwd'], 'string', 'max' => 20],
            [['access_token'], 'string', 'max' => 120],
            [['login'], 'unique'],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_stud' => Yii::t('app', 'Id Stud'),
            'id_tgroups' => Yii::t('app', 'Id Tgroups'),
            'fio_stud' => Yii::t('app', 'Fio Stud'),
            'adr_stud' => Yii::t('app', 'Adr Stud'),
            'phone_stud' => Yii::t('app', 'Phone Stud'),
            'birthdat_stud' => Yii::t('app', 'Birthdat Stud'),
            'spec_stud' => Yii::t('app', 'Spec Stud'),
            'learnstop_stud' => Yii::t('app', 'Learnstop Stud'),
            'dateback_stud' => Yii::t('app', 'Dateback Stud'),
            'comment_stud' => Yii::t('app', 'Comment Stud'),
            'img_stud' => Yii::t('app', 'Img Stud'),
            'price' => Yii::t('app', 'Price'),
            'form_pay' => Yii::t('app', 'Form Pay'),
            'id_1c' => Yii::t('app', 'Id 1c'),
            'login' => Yii::t('app', 'Login'),
            'passwd' => Yii::t('app', 'Passwd'),
            'free_vizit' => Yii::t('app', 'Free Vizit'),
            'photo_pas' => Yii::t('app', 'Photo Pas'),
            'user_last_date' => Yii::t('app', 'User Last Date'),
            'last_date_vizit' => Yii::t('app', 'Last Date Vizit'),
            'access_token' => Yii::t('app', 'Access Token'),
        ];
    }
}
