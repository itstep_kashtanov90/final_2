<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "tgroups".
 *
 * @property string $id_tgroups
 * @property string $id_streams
 * @property string $name_tgroups
 * @property string $arc_tgroups
 * @property integer $id_dir
 * @property integer $wiki_url
 * @property string $id_branch
 */
class Tgroups extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'tgroups';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_streams', 'arc_tgroups', 'id_dir', 'wiki_url', 'id_branch'], 'integer'],
            [['name_tgroups', 'id_dir'], 'required'],
            [['name_tgroups'], 'string', 'max' => 50]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_tgroups' => 'Id Tgroups',
            'id_streams' => 'Id Streams',
            'name_tgroups' => 'Name Tgroups',
            'arc_tgroups' => 'Arc Tgroups',
            'id_dir' => 'Id Dir',
            'wiki_url' => 'Wiki Url',
            'id_branch' => 'Id Branch',
        ];
    }

    public function getGroupsByStream($id_stream){
        $sql = "SELECT id_tgroups,name_tgroups FROM tgroups WHERE id_streams= :id_stream";
        return $response = self::getDb()->createCommand($sql)->bindValue(':id_stream', $id_stream)->queryAll();
    }

    public function getStreams(){
        $sql = "SELECT id_streams,name_streams FROM streams ORDER BY name_streams";
        return $response = self::getDb()->createCommand($sql)->queryAll();

    }

    public function getReviewsCount($student_id){
        $sql = "SELECT COUNT(*) FROM reviews WHERE student_id = :id_student";
        return self::getDb()->createCommand($sql)->bindValue(':id_student', $student_id)->queryScalar();
    }

    /**
     * this function is useful, when you need only list of students without some additional data
     * @param $id_tgroups
     * @return array|bool
     */
    public function getStudentsList($id_tgroups)
    {
        if(is_array($id_tgroups)){
            $where = 'id_tgroups IN (' . implode(",", $id_tgroups) . ')';
        }else{
            $where = "id_tgroups = :id_tgroups";
        }
        $query = "SELECT id_stud, fio_stud
                    FROM stud
                    WHERE " . $where . "
                    ORDER BY fio_stud ASC";
        if(!is_array($id_tgroups)) {
            $result = self::getDb()->createCommand($query)->bindValue(':id_tgroups', $id_tgroups)->queryAll();
        }else{
            $result = self::getDb()->createCommand($query)->queryAll();
        }
        if (!empty($result)){
            return $result;
        } else return false;
    }

    /**
     * this function you can use for get all active specs
     * @param $id_tgroups
     * @return array|bool
     */
    public function getActSpecsForGroup($id_tgroups)
    {
        $params = [
            //':id_tgroups' => $id_tgroups,
            ':id_teach' => $_SESSION['id_teach']
        ];
        $query = "SELECT DISTINCT id_spec, name_spec
                    FROM vizit
                    LEFT JOIN spec USING(id_spec)
                    WHERE id_teach = :id_teach
                    AND id_tgroups IN($id_tgroups)
                    AND id_spec>0
                    ORDER BY name_spec ASC";
        $result = self::getDb()->createCommand($query)->bindValues($params)->queryAll();
        if (!empty($result)){
            return $result;
        } else return false;
    }

    public function YearTextArg($year) {
        $year = abs($year);
        $t1 = $year % 10;
        $t2 = $year % 100;
        return ($t1 == 1 && $t2 != 11 ? \yii::t('model_tgroups','year') : ($t1 >= 2 && $t1 <= 4 && ($t2 < 10 || $t2 >= 20) ? \yii::t('model_tgroups','years') : \yii::t
        ('model_tgroups','years')));
    }

    public function getGroupName($id_tgroups){
        $sql = "SELECT  name_tgroups FROM tgroups WHERE id_tgroups= :id_tgroups";
        return $response = self::getDb()->createCommand($sql)->bindValue(':id_tgroups', $id_tgroups)->queryOne();
    }

    public function getPoints($id_stud){
        $sql = "SELECT stud_points,soc_points FROM gaming_student WHERE id_stud= :id_stud";
        $response = self::getDb()->createCommand($sql)->bindValue(':id_stud', $id_stud)->queryOne();

        if(!isset($response['stud_points'])) $response['stud_points'] = 0;
        if(!isset($response['soc_points'])) $response['soc_points'] = 0;
        return $response;
    }

    public function getGroups(){
        $sql = "SELECT id_tgroups FROM tgroups WHERE arc_tgroups=0 ORDER BY name_tgroups";
        return self::getDb()->createCommand($sql)->queryAll();
    }

    public function getActGroups($id_teach = false){
        $teach_and = "";
        $params = [];
        $join = "INNER JOIN";
        if ($id_teach){
            $teach_and = " AND vizit.id_teach = :id_teach";
            $params[':id_teach'] = $id_teach;
            $join = "RIGHT JOIN";
        }
        $sql = "SELECT DISTINCT tgroups.id_tgroups,tgroups.name_tgroups
                FROM tgroups
                $join vizit
                ON tgroups.id_tgroups = vizit.id_tgroups
                WHERE
                arc_tgroups = 0
                $teach_and
                ORDER BY tgroups.name_tgroups";

        //echo $sql;//exit;

        return $response = self::getDb()->createCommand($sql)->bindValues($params)->queryAll();
    }

    public function getStudent($id_stud){
        $sql = "SELECT  id_form, spec_stud,adr_stud,stud.`id_stud`,fio_stud,birthdat_stud,photo_pas, st_email,st_mob1,
                        st_mob2,vk, facebook,twitter,tgroups.name_tgroups,name_streams, rel_1_email, rel_2_email,
                        rel_3_email
                FROM stud 
                LEFT JOIN stud_ex ON stud_ex.`id_stud` = stud.`id_stud`
                LEFT JOIN tgroups USING (id_tgroups)
                LEFT JOIN streams USING (id_streams)
                WHERE stud.`id_stud`= :id_stud";
        return $response = self::getDb()->createCommand($sql)->bindValue(':id_stud', $id_stud)->queryOne();
    }
}