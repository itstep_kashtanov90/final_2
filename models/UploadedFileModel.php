<?php
/**
 * Created by PhpStorm.
 * User: kashtanov
 * Date: 08.04.2016
 * Time: 11:25
 */

namespace app\models;

use yii\helpers\Html;
use yii\base\Model;
use yii\web\UploadedFile;

/**
 * UploadForm is the model behind the upload form.
 */
class UploadedFileModel extends Model
{

    public $file;

    private $path = "load_portfolio/";

    public function rules()
    {
        return [
//            [['file'], 'required', 'message' => '���������� ������� ����!'],
            [['file'], 'file', 'skipOnEmpty' => false],
        ];
    }

    public function SaveAndGetNameFile($id_stud){

        $file_name = $this->file->baseName . '.' . $this->file->extension;

        $prefix = BaseModel::getPrefix();

        $full_path = $this->path . "$prefix/$id_stud";

        if($this->Search($full_path, $file_name)){
            $i = 1;
            for ( ; ; ) {

                $new_file_name = $i . "_" . $file_name;

                if(!$this->Search($full_path, $new_file_name)){
                    $file_name = $new_file_name;
                    break;
                }

                $i++;
            }
        }

//        MyDebug::P([
//            $this->getRandomFileName($full_path, $this->file->extension),
//            $this->file->extension,
//            $id_stud
//            ],1);

        if ($this->file && $this->validate()) {
            $this->file->saveAs("$full_path/$file_name", false);
        }

        return $file_name;
    }

    private function Search($full_path, $file_name){

        if(file_exists($full_path)){
            $scan = scandir($full_path);
            if(count($scan) >= 1) {
                foreach($scan as $name){
                    if($file_name == $name){
                        return true;
                    }
                }
            }
        } else {
            mkdir($full_path, 0777, true);
        }

        return false;
    }



    public static function getRandomFileName($path, $extension = false)
    {
        $extension = $extension ? '.' . $extension : '';
        $path = $path ? $path . '/' : '';

        do {
            $name = md5(microtime() . rand(0, 9999));
            $file = $path . $name . $extension;
        } while (file_exists($file));

        return $name . $extension;
    }

    public function GetPath(){
        $prefix = BaseModel::getPrefix();
        return $this->path . $prefix . "/";
    }
}