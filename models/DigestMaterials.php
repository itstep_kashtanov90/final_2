<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "digest_materials".
 *
 * @property integer $id_digest_materials
 * @property integer $id_digest
 * @property integer $id_teach
 * @property integer $id_stud
 * @property string $comment
 * @property string $file_name
 * @property string $path
 * @property string $type
 * @property string $date
 *
 * @property DigestModel $digestMaterials
 */
class DigestMaterials extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'digest_materials';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_digest', 'id_teach', 'id_stud', 'path', 'type'], 'required'],
            [['id_digest', 'id_teach', 'id_stud'], 'integer'],
            [['comment', 'file_name', 'path', 'type'], 'string'],
            [['date'], 'safe'],
            [['id_digest'], 'exist', 'skipOnError' => true, 'targetClass' => DigestModel::className(), 'targetAttribute' => ['id_digest' => 'id_digest']],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_digest_materials' => Yii::t('app', 'Id Digest Materials'),
            'id_digest' => Yii::t('app', 'Digest Materials'),
            'id_teach' => Yii::t('app', 'Id Teach'),
            'id_stud'  => Yii::t('app', 'Id stud'),
            'comment' => Yii::t('app', 'Comment'),
            'file_name' => Yii::t('app', 'File Name'),
            'path' => Yii::t('app', 'Path'),
            'type' => Yii::t('app', 'Type'),
            'date' => Yii::t('app', 'Date'),
        ];
    }

    /**
     * @return \yii\db\ActiveQuery
     */
    public function getDigestMaterials()
    {
        return $this->hasOne(DigestModel::className(), ['id_digest' => 'id_digest']);
    }
}
