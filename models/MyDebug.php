<?php
/**
 * Created by PhpStorm.
 * User: kashtanov
 * Date: 29.02.2016
 * Time: 17:56
 */

namespace app\models;


class MyDebug
{

    public static function P($data, $exit = false){
        echo '<pre>';
        print_r($data);
        echo '</pre>';
        if($exit) exit;
    }

    public static function C($data, $group = false, $g = 0, $f = 1){

        if(is_array($data)){
            if($f){
                $text = $group ? $group : 'count - ' . count($data);
                echo "<script> console.group('$text'); </script>";
            }
            foreach($data as $key => $val){
                if(is_array($val)){
                    echo "<script> console.groupCollapsed('key - $key'); </script>";
                    self::C($val, false, 0, false);
                    echo "<script> console.groupEnd(); </script>";
                } else {
                    $g_ = '';
                    $val = str_replace("'",'"',$val);
                    $val = str_replace('\r\n','',$val);
                    $PCREpattern = '/\r\n|\r|\n/u';

                    $val = preg_replace($PCREpattern, '', $val);

                    if(is_int($key) || $key == '0'){
                        echo "<script> var test_subject = '$val'; console.log($key + ' - ' + test_subject); </script>";
                    } else {
                        echo "<script> console.info('$key'); </script>";
                        echo "<script>
                             var test_subject = '$val';

                             console.log(test_subject);
                     </script>";
                    }
                }
            }
            if($f) echo "<script> console.groupEnd(); </script>";
        } else {
            $g_ = '';
            for($i=0;$i<$g;$i++){ $g_ .= ' - '; }
            echo "<script> console.log(\"$g_ $data\"); </script>";
        }
    }

    public static function CE($val){
        echo "<script> console.error(`$val`); </script>";
    }

    public static function CiG($val = null){
        if(is_null($val)){
            $val = new \DateTime();
            $val = $val->format("H:i:s");
        }
        echo "<script> console.group(`$val`); </script>";
    }

    public static function CoG(){
        echo "<script> console.groupEnd(); </script>";
    }
}