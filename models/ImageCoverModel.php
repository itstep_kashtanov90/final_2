<?php
/**
 * Created by PhpStorm.
 * User: kashtanov
 * Date: 10.06.2016
 * Time: 15:37
 */

namespace app\models;

use yii\helpers\Html;

class ImageCoverModel
{

    public static function getCover($expansion_file){

        $directory = "../web/public/image/covers/$expansion_file.png";

        if(file_exists($directory)){
            $file = Html::img("../public/image/covers/$expansion_file.png");
        } else {
            $file = "";//"../img/folder.png";
        }

        return $file;
    }
}