<?php
/**
 * Created by PhpStorm.
 * User: kashtanov
 * Date: 06.04.2016
 * Time: 12:04
 */

namespace app\models;

use Yii;
use yii\db\ActiveRecord;
use yii\helpers\Html;

/**
 * This is the model class for table "digest".
 *
 * @property integer $id_digest
 * @property integer $id_stud
 * @property integer $id_teach
 * @property integer $id_tgroups
 * @property integer $mark
 * @property integer $comment
 * @property string $name
 * @property string $file_name
 * @property string $path
 * @property string $type
 * @property string $date
 */

class DigestModel extends ActiveRecord
{

    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'digest';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_stud', 'id_teach', 'id_tgroups', 'mark', 'name', 'path'], 'required'],
            [['id_stud', 'id_teach', 'id_tgroups', 'mark'], 'integer'],
            [['path', 'type', 'comment'], 'string'],
            [['date'], 'safe'],
            [['name','file_name'], 'string', 'max' => 100],
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_digest'     => Yii::t('app', 'id работы'),
            'id_stud'       => Yii::t('app', 'ФИО студента'),
            'id_teach'      => Yii::t('app', 'ФИО Преподавателя'),
            'id_tgroups'    => Yii::t('app', 'Название группы'),
            'mark'          => Yii::t('app', 'Оценка'),
            'comment'       => Yii::t('app', 'Описание  работы'),
            'name'          => Yii::t('app', 'Название работы'),
            'file_name'     => Yii::t('app', 'Название Файла'),
            'path'          => Yii::t('app', 'Путь к файлу работы ( зависит от типа )'),
            'type'          => Yii::t('app', 'Тип работы ( Файл, видео, презентация... )'),
            'date'          => Yii::t('app', 'Дата публикации'),
            'stud.fio_stud' => '',
        ];
    }

    /*
     *  GET:            Stud    By   ID   Groups
     */
    public function getStudByGroups($id_tgroups){

        $params[':id_tgroups'] = $id_tgroups;

        $sql = "SELECT id_stud, fio_stud
                FROM stud
                WHERE
                id_tgroups = :id_tgroups
                ORDER BY fio_stud";

        return $response = self::getDb()->createCommand($sql)->bindValues($params)->queryAll();
    }

    public function getAllPortfolio(){
        $sql = "SELECT digest.*, stud.fio_stud
                FROM logbook_dp.digest
                LEFT JOIN logbook_dp.stud USING (id_stud)
                ";
        $response = self::getDb()->createCommand($sql)->queryAll();
        return $response;
    }

    /*
     *  GET:            Portfolio
     */
    public function getPortfolio($sel = false, $val = false){

        switch($sel){
            case "id_stud":

                // GET        --- BY ___ ID ___ STUD

                $param = [
                    ":id_stud" => $val
                ];
                $sql = "SELECT digest.*, fio_stud, name_tgroups, fio_teach
                FROM digest
                LEFT JOIN stud USING (id_stud)
                LEFT JOIN teach USING (id_teach)
                LEFT JOIN tgroups ON (tgroups.id_tgroups = digest.id_tgroups)
                WHERE digest.id_stud = :id_stud";
                $response = self::getDb()->createCommand($sql)->bindValues($param)->queryAll();
                break;
            default:

                // GET        --- DEFAULT ___ ALL ___ STUD

                $sql = "SELECT id_stud, fio_stud, name, mark
                FROM digest
                LEFT JOIN stud USING (id_stud)
                ";//GROUP BY id_stud
                $response = self::getDb()->createCommand($sql)->queryAll();

                $data = [];

                foreach($response as $val){
                    $data[$val['id_stud']]['works'][] = [ 'name' => $val['name'], 'mark' => $val['mark'] ];
                    $data[$val['id_stud']]['fio_stud'] = $val['fio_stud'];
                }

                $response = $data;

                break;
        }

        return $response;
    }

    /*
     *  TODO:                    Path    City
     */
    public static function GetPathCity($http = true){

        $prefix = BaseModel::getPrefix();

        $server = 'final.itstep.org';

        if ( isset($_SERVER['WINDIR']) ) {
            $server = 'final';
        }

        $path = ($http ? "http://" : "") . "$server/load_portfolio/$prefix/";

        return $path;
    }

    /**
     * @inheritdoc
     * @return DigestQuery the active query used by this AR class.
     */
//    public static function find()
//    {
//        return new DigestQuery(get_called_class());
//    }
}