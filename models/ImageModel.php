<?php
/**
 * Created by PhpStorm.
 * User: kashtanov
 * Date: 11.05.2016
 * Time: 12:43
 */

namespace app\models;

use yii\helpers\Html;
use yii\base\Model;
use yii\web\UploadedFile;


class ImageModel extends Model
{
    /**
     * @var UploadedFile file attribute
     */
    public $file;

    private $path = "load_portfolio/";

    public function rules()
    {
        return [
            [['file'], 'required', 'message' => 'Необходимо выбрать файл!'],
            [['file'], 'file', 'skipOnEmpty' => false],
            //[['file'], 'file', 'extensions' => 'png, jpg, jpeg', 'message' => 'Доступное расширение файла png, jpg, jpeg!'],
            [['file'], 'file', 'maxSize' => 1024 * 1024 * 2 ],

        ];
    }

    public function attributeLabels()
    {
        return [
            'file' => 'Файл',
        ];
    }

    public function GetPath(){
        $prefix = BaseModel::getPrefix();
        return $this->path . $prefix . "/";
    }
}