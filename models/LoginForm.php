<?php

namespace app\models;

use Yii;
use yii\base\Model;
use yii\db\ActiveRecord;
use yii\helpers\ArrayHelper;

/**
 * Логин студентов MyStat
 */
// class LoginForm extends Model
class LoginForm extends ActiveRecord
{
    public $username;
    public $password;
    public $remember;

    public $list_city = [ ];

    /**
     * @return array the validation rules.
     */
    public function rules()
    {
        return [
            // username and password are both required
            [['list_city'], 'required'],
            [['username'], 'required','message'=>\Yii::t('loginform','field_login')],
            [['password'], 'required','message'=>\Yii::t('loginform','field_pass')],
            // password is validated by validatePassword()
        ];
    }

    public function attributeLabels()
    {
        return [
            'username'=>'Логин',
            'password'=>'Пароль',
            'remember'=>'Запомнить'
        ];
    }

    public function __construct()
    {
        $password = 'skdlelwru894urhjkari83A';
        $username = 'logbook2';

        $dsn = 'mysql:host=localhost;dbname=logbook_public';

        if (isset($_SERVER['WINDIR'])) { // если локально
            $dsn = 'mysql:host=localhost;dbname=logbook_public';
            $username = 'root';
            $password = '';
        }

        $connection = new \yii\db\Connection([
            'dsn' => $dsn,
            'username' => $username,
            'password' => $password,
            'charset' => 'utf8'
        ]);

        $connection->open();
        $query = "SELECT id_city, prefix FROM logbook_public.public_city";
        $this->list_city = $connection->createCommand($query)->queryAll();

        foreach ( $this->list_city as $key => $city) { // перебираем массив префиксов городов с таблички

            $this->list_city[$key]['name'] = \Yii::t('default', $city['prefix'] ); //префикс города с таблички
            // выступает в роли ключа для перевода

        }


        array_unshift($this->list_city, ['id_city'=>'city_select', 'name'=>\Yii::t('loginform','sel_city')]);
        $this->list_city = ArrayHelper::map($this->list_city, 'id_city',  'name');//prepare for DropDownList
    }

}
