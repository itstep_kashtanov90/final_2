<?php
/**
 * Created by PhpStorm.
 * User: kashtanov
 * Date: 04.05.2016
 * Time: 9:38
 */

namespace app\models;

use Yii;
use yii\db\ActiveRecord;

class BaseModel extends ActiveRecord
{

    public static function count_array($array)//delete?? we dont use it
    {
        return count($array);
    }

    public static function deleteTags($value) {
        $value = str_replace("+", "%2B", $value);
        $value = str_replace(",", "%2C", $value);
        $value = str_replace("#", "%23", $value);
        return $value;
    }

    public static function getUnfilledDate() {
        return (new \DateTime())->diff((new \DateTime('28-04-2015')))->format('%d');
    }

    public static function getPrefix()
    {
        $query = "SELECT prefix
                    FROM logbook_public.public_city
                    WHERE id_city = :id_city";

        $prefix = \Yii::$app->getDb()->createCommand($query)->bindValue(':id_city', $_SESSION['city_id'])->queryScalar();
        return $prefix;
    }

    public static function getPrefixByCityId($city_id)
    {
        $query = "SELECT prefix
                    FROM logbook_public.public_city
                    WHERE id_city = :id_city";

        $prefix = \Yii::$app->getDb()->createCommand($query)->bindValue(':id_city', $city_id)->queryScalar();
        return $prefix;
    }

    /**
     * time correction is according to timezone of Ukraine
     * @return int
     */
    public static function getTimezoneByCityId()
    {
        $query = "SELECT timezone
                    FROM logbook_public.public_city
                    WHERE id_city = :id_city";
        $timezone =\Yii::$app->getDb()->createCommand($query)->bindValue(':id_city', $_SESSION['city_id'])
            ->queryScalar();
        /***correction for winter time*/
        if ($timezone==3){
            $timezone-=1;
        }

        if (isset($_SERVER['WINDIR'])){
            $timezone-=3;
        }
        return $timezone;
    }

    public static function clearThemeVizit($str)
    {
        return str_replace(["[]", "[", "]"], "", $str);
    }

    public static function nameCityById($id)
    {
        $query = "SELECT name
                    FROM logbook_public.public_city
                    WHERE id_city = :id_city";
        $name = \Yii::$app->getDb()->createCommand($query)->bindValue(':id_city', $id)->queryScalar();
        return $name;
    }

    /**
     * @return \DateTime with correction according to TimeZone
     */
    public static function getCurrentDateTime()
    {
        $time= new \DateTime();
        $time_zone = self::getTimezoneByCityId();
        $interval=new \DateInterval("PT".abs($time_zone)."H");
        $fix_time=$time_zone>=0?$time->add($interval):$time->sub($interval);
        return $fix_time;
    }
}