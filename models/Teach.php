<?php

namespace app\models;

use Yii;
use yii\db\Query;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\helpers\M_HtmlFilter;
use yii\web\UploadedFile;
use yii\web\Session;

/**
 *
 * @property string $id_teach
 * @property string $fio_teach
 * @property string $adr_teach
 * @property string $homephone_teach
 * @property string $mobilphone_teach
 * @property string $addphone_teach
 * @property string $email_teach
 * @property string $corp_email_teach
 * @property string $birthdat_teach
 * @property string $skype_teach
 * @property string $arc_teach
 * @property string $spec_teach
 * @property string $even_odd
 * @property resource $photo_teach
 * @property string $cert_teach
 * @property string $edu_teach
 * @property integer $alt_teach
 * @property string $post_teach
 * @property integer $open_days
 * @property string $photo_pas
 * @property integer $mystat
 */
class Teach extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'teach';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['birthdat_teach'], 'safe'],
            [['arc_teach', 'even_odd', 'alt_teach', 'open_days', 'mystat'], 'integer'],
            [['spec_teach'], 'required'],
            [['spec_teach', 'photo_teach', 'cert_teach', 'photo_pas'], 'string'],
            [['fio_teach', 'homephone_teach', 'mobilphone_teach', 'addphone_teach'], 'string', 'max' => 50],
            [['adr_teach'], 'string', 'max' => 100],
            [['email_teach', 'corp_email_teach'], 'string', 'max' => 80],
            [['skype_teach'], 'string', 'max' => 20],
            [['edu_teach', 'post_teach'], 'string', 'max' => 255]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id_teach'         => 'Id Teach',
            'fio_teach'        => 'Fio Teach',
            'adr_teach'        => 'Adr Teach',
            'homephone_teach'  => 'Homephone Teach',
            'mobilphone_teach' => 'Mobilphone Teach',
            'addphone_teach'   => 'Addphone Teach',
            'email_teach'      => 'Email Teach',
            'corp_email_teach' => 'Corp Email Teach',
            'birthdat_teach'   => 'Birthdat Teach',
            'skype_teach'      => 'Skype Teach',
            'arc_teach'        => 'Arc Teach',
            'spec_teach'       => 'Spec Teach',
            'even_odd'         => 'Even Odd',
            'photo_teach'      => 'Photo Teach',
            'cert_teach'       => 'Cert Teach',
            'edu_teach'        => 'Edu Teach',
            'alt_teach'        => 'Alt Teach',
            'post_teach'       => 'Post Teach',
            'open_days'        => 'Open Days',
            'photo_pas'        => 'Photo Pas',
            'mystat'           => 'Mystat',
        ];
    }

    public static function login($username, $password)
    {
        // var_dump($_SESSION['city_id']);die;
        date_default_timezone_set('UTC');
        $psw       = $password;
        $pas_query = 'old_password(:pass)';
        if ($_SESSION['city_id'] == 8) {
            $pas_query = 'password(:pass)';
        }
        $user = self::getDb()->createCommand("SELECT teach.fio_teach, teach.id_teach, arc_teach, IFNULL(corp_email_teach,email_teach) as email,photo_pas, user.role FROM user, teach
                         WHERE username= :username
                         AND passwd = " . $pas_query . "
                         AND user.id_teach = teach.id_teach
                         AND arc_teach = 2
                         AND role = 3")->bindValues([':username' => $username, ':pass' => $psw])->queryOne();
        if (!empty($user)) {//logic is from old logbook, in future may be will be changed
            $_SESSION['id_teach'] = $user['id_teach'];
            $_SESSION['email'] = $user['email'];
            $_SESSION['fio_teach'] = $user['fio_teach'];
            $_SESSION['photo_pas'] = $user['photo_pas'];
            $_SESSION['role'] = $user['role'];

            $ext_ip = (!in_array($_SERVER['HTTP_CF_CONNECTING_IP'], array("195.24.136.102", "212.3.124.250"))) ?
                true :
                false;
            if ((new Teach())->isTeachCheckedIn($_SESSION['id_teach']) == 0 && !$ext_ip) {
                if (true) {
                    $timezone = BaseModel::getTimezoneByCityId();
                    if ($timezone >= 0) {
                        $act_date = (new \DateTime())->add(new \DateInterval('PT0' . $timezone . 'H'))->format("Y-m-d H:i:s");
                    } else {
                        $timezone = abs($timezone);
                        $act_date = (new \DateTime())->sub(new \DateInterval('PT0' . $timezone . 'H'))->format("Y-m-d H:i:s");
                    }
                    $_SESSION['time_reg'] = (new \DateTime($act_date))->format("H:i:s");

                    /*try to find first pair for today for actual teach*/

                    $base = "logbook_" . BaseModel::getPrefix();

                    $query = "SELECT *
                            FROM $base.rasp
                            WHERE (
                            WEEKDAY( NOW( ) ) +1) = nday
                            AND id_teach = :id_teach";
                    $result = self::getDb()->createCommand($query)->bindValue('id_teach', $user['id_teach'])->queryOne();
                    $rasp_array = ArrayHelper::toArray($result);
                    $lenta = -1;
                    for ($i = 0; $i <= 9; $i++) {
                        if ($rasp_array[0]['id_tgroups' . $i] != NULL) {
                            $lenta = $i;
                            break;
                        }
                    }
                    $time_rasp = '00:00:00';
                    if ($lenta != -1) {
                        $query = "SELECT l_start
                                FROM $base.rasp_lenta
                                WHERE n_lenta= :lenta";
                        $result = self::getDb()->createCommand($query)->bindValue('lenta', $lenta)->query();
                        $rasp_lenta_array = ArrayHelper::toArray($result);
                        $time_rasp = $rasp_lenta_array[0]['time_rasp'];
                    }

                    $base = "register_" . BaseModel::getPrefix();
                    $ip_reg = $_SERVER['REMOTE_ADDR'];

                    self::getDb()->createCommand()->insert($base . ".time",
                        [
                            'id_teach'  => $user['id_teach'],
                            'date_reg'  => $act_date,
                            'time_rasp' => $time_rasp,
                            'ip_reg'    => $ip_reg
                        ]
                    )->execute();
                }
            } else if ($ext_ip) {
                $_SESSION['time_reg'] = "Внешний IP: " . $_SERVER['REMOTE_ADDR'];

            }
        }
        return $user;
    }

    public function isTeachCheckedIn($id_teach)
    {
        $base = "register_" . BaseModel::getPrefix();
        $query = "SELECT date_reg
                FROM  $base.time
                WHERE DATE( date_reg ) = CURRENT_DATE
                AND id_teach= :id_teach";
        $result = self::getDb()->CreateCommand($query)->bindValue(':id_teach', $id_teach)->queryAll();
        $reg_array = ArrayHelper::toArray($result);
        $num_rows = count($reg_array);
        $_SESSION['time_reg'] = (new \DateTime($reg_array[0]['date_reg']))->format("H:i:s");

        return $num_rows;
    }

    public static function getTeach($id_teach)
    {
        $loginTeach = self::getDb()->createCommand("SELECT teach.fio_teach, teach.id_teach, IFNULL(corp_email_teach,email_teach) as email,photo_pas
                         FROM user, teach
                         WHERE teach.id_teach= :id_teach
                         AND user.id_teach = teach.id_teach
                         AND arc_teach <> 11
                         AND arc_teach <> 1")->bindValue(':id_teach', $id_teach)->queryOne();

        if (!empty($loginTeach)) {
            $_SESSION['id_teach'] = $loginTeach['id_teach'];
            $_SESSION['email'] = $loginTeach['email'];
            $_SESSION['fio_teach'] = $loginTeach['fio_teach'];
            $_SESSION['photo_pas'] = $loginTeach['photo_pas'];
        }
        return $loginTeach;
    }

    /**
     * @param $id_teach
     * @return array|bool
     */
    public static function getTeachData($id_teach)
    {
        $query = "SELECT fio_teach, post_teach, birthdat_teach, homephone_teach, mobilphone_teach, addphone_teach, email_teach, corp_email_teach, adr_teach
                FROM teach
                WHERE id_teach= :id_teach";
        /** @var array $result */
        $result = self::getDb()->createCommand($query)->bindValue('id_teach', $id_teach)->queryOne();

        if (!empty($result)) {
            $teach_data = ArrayHelper::toArray($result);
            /** @var array|bool $teach_data */
            return $teach_data;
        } else return false;
    }

    public static function getTeachSpec($id_teach)
    {
        #get teach's specs
        $query = "SELECT spec_teach
                        FROM teach
                        WHERE id_teach= :id_teach";
        $teach_specs_array = self::getDb()->createCommand($query)->bindValue(':id_teach', $id_teach)->queryColumn();
        $teach_specs_array = preg_split('/[\[\]]/', $teach_specs_array[0], -1, PREG_SPLIT_NO_EMPTY);

        #If teach have no specs, then return empty array for prevent crash of page
        if (empty($teach_specs_array)) return [];

        return implode(',', $teach_specs_array);
    }

    public static function getTeachEmail($email)
    {
        $sql = "SELECT teach.id_teach ,teach.corp_email_teach, teach.email_teach
                FROM teach
                          INNER JOIN user USING (id_teach)
                          WHERE corp_email_teach = :email OR email_teach = :email";

        return self::getDb()->createCommand($sql)->bindValue(':email', $email)->queryOne();
    }

    public static function RecoverPassword($id_teach, $pass)
    {
        $password = self::getDb()->createCommand("SELECT OLD_PASSWORD( :pass)")->bindValue(':pass', $pass)->queryScalar();
        if ($_SESSION['city_id'] == 8) // для Киева
        {
            $password = self::getDb()->createCommand("SELECT password( :pass)")->bindValue(':pass', $pass)->queryScalar();
        }
        return self::getDb()->createCommand()->update("user", ['passwd' => $password], "id_teach = $id_teach")->execute();
    }

    public function changePassword($id_teach, $pass, $pass_repeat)
    {

        if (empty($pass) || empty($pass_repeat)) {
            $result = "not_empty_pass";
        } elseif ($pass != $pass_repeat) {
            $result = "pass_not_match";

        } else {
            $sql = "SELECT OLD_PASSWORD( :pass)";
            $password = self::getDb()->createCommand($sql)->bindValue(':pass', $pass)->queryScalar();
            if ($_SESSION['city_id'] == 8) // для Киева
            {
                $sql = "SELECT password( :pass)";
                $password = self::getDb()->createCommand($sql)->bindValue(':pass', $pass)->queryScalar();
            }
            $result = self::getDb()->createCommand()->update("user", ['passwd' => $password], "id_teach = $id_teach")
                ->execute();

        }
        return $result;
    }

    public static function isSuperAdmin($user_id){

        $query = new Query();

        $query
            ->select('superadmin')
            ->from('user')
            ->where('id_teach = :user')
            ->params(['user' => $user_id]);

        return $query->scalar();
    }

}
