<?php

namespace app\models;

use Yii;

/**
 * This is the model class for table "logbook_public.public_city".
 *
 * @property integer $id
 * @property integer $id_city
 * @property string $prefix
 * @property string $name
 * @property integer $timezone
 * @property string $mask_landline
 * @property string $mask_mobline
 * @property integer $active
 *
 */
class PublicCity extends \yii\db\ActiveRecord
{
    /**
     * @inheritdoc
     */
    public static function tableName()
    {
        return 'logbook_public.public_city';
    }

    /**
     * @inheritdoc
     */
    public function rules()
    {
        return [
            [['id_city', 'prefix', 'name', 'active'], 'required'],
            [['id_city', 'timezone', 'active'], 'integer'],
            [['prefix'], 'string', 'max' => 10],
            [['name', 'mask_landline', 'mask_mobline'], 'string', 'max' => 30]
        ];
    }

    /**
     * @inheritdoc
     */
    public function attributeLabels()
    {
        return [
            'id' => Yii::t('app', 'ID'),
            'id_city' => Yii::t('app', 'Id City'),
            'prefix' => Yii::t('app', 'Prefix'),
            'name' => Yii::t('app', 'Name'),
            'timezone' => Yii::t('app', 'Timezone'),
            'mask_landline' => Yii::t('app', 'Mask Landline'),
            'mask_mobline' => Yii::t('app', 'Mask Mobline'),
            'active' => Yii::t('app', 'Active'),
        ];
    }
}
