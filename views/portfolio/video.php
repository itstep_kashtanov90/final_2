<?php
/**
 * Created by PhpStorm.
 * User: kashtanov
 * Date: 31.05.2016
 * Time: 14:41
 */

?>

<?php if(preg_match("/(([a-z0-9\-\.]+)?[a-z0-9\-]+(!?\.[a-z]{2,4}))/", $prepared_path)){ ?>

    <iframe
        class="img-rounded img-thumbnail"
        width="500"
        height="500"
        src="<?= $prepared_path ?>"
        frameborder="0"
        webkitAllowFullScreen
        mozallowfullscreen
        allowfullscreen>
    </iframe>

<?php } else { ?>

    <div class="thumb" style="background-image: url('../public/image/covers/video_not_found.png')">
        <?//= ImageCoverModel::getCover($expansion) ?>
    </div>

<?php } ?>
