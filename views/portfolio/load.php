<?php
/**
 * Created by PhpStorm.
 * User: kashtanov
 * Date: 05.04.2016
 * Time: 14:21
 */
use yii\helpers\Html;
use yii\widgets\ActiveForm;
?>
    <h3>Загрузить работу</h3>
    <?php $form = ActiveForm::begin([
        'method' => 'post',
        'action' => 'new',
        'options' => [
            'enctype' => 'multipart/form-data',
        ],
        'fieldConfig' => [
            'template' => "<div class='label_error' style='display: block;'> {error} </div><div> {input} </div>",
        ],
    ]); ?>
    <div class="row">
        <div class="col-md-2 label-div-form">
            <?= \Yii::t('js_superusser', 'sel_teach') ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'id_teach')->dropDownList($all_teach, [
                'id'    => 'teach',
                'class' => 'form-control font_opensans select_form2 inline_block',
            ]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-2 label-div-form">
            <?= \Yii::t('progressgroup_index','sel_group') ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'id_tgroups')->dropDownList(['' => \Yii::t('progressgroup_index','sel_group')], [
                'id'    => 'groups',
                'class' => 'form-control font_opensans select_form2 inline_block',
            ]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-2 label-div-form">
            <?= \Yii::t('js_superusser','sel_stud') ?>
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'id_stud')->dropDownList(['' => \Yii::t('js_superusser','sel_stud')], [
                'id'    => 'stud',
                'class' => 'form-control font_opensans select_form2 inline_block',
            ]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-2 label-div-form">
            Название работы:
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'name')->input('text', [
                'class'         => 'form-control font_opensans select_form2 inline_block',
                'placeholder'   => 'Название работы',
            ]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-2 label-div-form">
            Оценка за работу:
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'mark')->dropDownList($all_marks, [
                'class' => 'form-control font_opensans select_form3',
            ]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-2 label-div-form">
            Описание работы:
        </div>
        <div class="col-md-3">
            <?= $form->field($model, 'comment')->textarea([
                'class'         => 'form-control font_opensans select_form2 inline_block',
                'placeholder'   => 'Комментарий',
            ]) ?>
        </div>
    </div>
    <div class="row">
        <div class="col-md-2 label-div-form">
            Обложка для работы:
        </div>
        <div class="col-md-3">
            <?= $form->field($file_model, 'file')->fileInput([]) ?>
        </div>
    </div>

    <div class="row">
        <div class="col-md-2">
            <?= Html::submitInput('Отправить', [
                'class' => 'btn btn-success'
            ]) ?>
        </div>
    </div>

<?php ActiveForm::end(); ?>