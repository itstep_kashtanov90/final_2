<?php

/**
 * Created by PhpStorm.
 * User: kashtanov
 * Date: 18.04.2016
 * Time: 13:43
 */
use yii\helpers\Html;
use app\models\DigestModel;
use yii\widgets\ActiveForm;

?>

<?= newerton\fancybox\FancyBox::widget([
    'target' => 'a[rel=fancybox]',
    'helpers' => true,
    'mouse' => true,
    'config' => [
        'maxWidth' => '90%',
        'maxHeight' => '90%',
        'playSpeed' => 7000,
        'padding' => 0,
        'fitToView' => false,
        'width' => '70%',
        'height' => '70%',
        'autoSize' => false,
        'closeClick' => false,
        'openEffect' => 'elastic',
        'closeEffect' => 'elastic',
        'prevEffect' => 'elastic',
        'nextEffect' => 'elastic',
        'closeBtn' => false,
        'openOpacity' => true,
        'helpers' => [
            'title' => ['type' => 'float'],
            'buttons' => [],
            'thumbs' => ['width' => 68, 'height' => 50],
            'overlay' => [
                'css' => [
                    'background' => 'rgba(0, 0, 0, 0.8)'
                ]
            ]
        ],
    ]
]); ?>

<h3>Редактирование работы</h3>
<div class="row">
    <div class="col-md-12">
        <div class="thumbnail">
            <div class="row">
                <div class="col-md-3">
                    <?php
                        switch($digest->type){
                            case "image": ?>
                                <div class="example2" style="padding: 3em 0;">
                                    <?php $prepared_path = DigestModel::GetPathCity() . $digest->id_stud . "/" . $digest->path; ?>
                                    <?= Html::a( Html::img($prepared_path), $prepared_path, ['rel' => 'fancybox'] ); ?>
<!--                                <img-->
<!--                                    class="img-rounded img-thumbnail"-->
<!--                                    src='--><?//= DigestModel::GetPathCity() . $digest->id_stud . "/" . $digest->path ?><!--' />-->
                                </div>
                                <?php break;
                            case "video":
                                echo '<iframe class="img-rounded img-thumbnail" width="300" height="250" src="'. $digest->path .'" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowfullscreen></iframe>';
                                break;
                            case "pdf": ?>
                                <iframe class="img-rounded img-thumbnail" width="350" height="250" src="<?= DigestModel::GetPathCity() . $digest->id_stud . "/" . $digest->path ?>" frameborder="0" webkitAllowFullScreen mozallowfullscreen allowfullscreen></iframe>
                                <?php break;
                            case "file": ?>
                                <div class="example2">
                                    <img src="../img/folder.png" class="img-rounded img-thumbnail example_beauty" style="height: 250px; width: 350px;">
                                    <span class="img-rounded" style="width: 290px; object-fit: cover; "><?= $digest->file_name ?></span>
                                </div>
                                <?php break;
                        }
                    ?>
                </div>
                <div class="col-md-9" style="padding-top: 15px;">
                    <div class="row">
                        <div class="col-md-9">
                            <div class="row">
                                <div class="col-md-3">
                                    ФИО студента:
                                </div>
                                <div class="col-md-6">
                                    <strong><?= $stud->fio_stud ?></strong>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    Название работы:
                                </div>
                                <div class="col-md-6">
                                    <strong data-digest-el="name" data-digest-id="<?= $digest->id_digest ?>" data-this_active="true">
                                        <?= $digest->name ?>
                                        <i class="glyphicon glyphicon-pencil" aria-hidden="true"></i>
                                    </strong>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    Оценка:
                                </div>
                                <div class="col-md-6">
                                    <strong data-digest-el="mark" data-digest-id="<?= $digest->id_digest ?>" data-this_active="true">
                                        <?= $digest->mark ?>
                                        <i class="glyphicon glyphicon-pencil" aria-hidden="true"></i>
                                    </strong>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col-md-3">
                                    Описание:
                                </div>
                                <div class="col-md-7">
                                    <strong data-digest-el="comment" data-digest-id="<?= $digest->id_digest ?>" data-this_active="true">
                                        <?= $digest->comment ?>
                                        <i class="glyphicon glyphicon-pencil" aria-hidden="true"></i>
                                    </strong>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3">
                            <form id="target" method="post" action="delete-digest">
                                <input type="hidden" value="<?= $digest->id_digest ?>" name="id_digest" >
                                <input type='submit' class="btn btn-danger pull-right" style="margin-right: 15px;" value="Удалить Дайжест">
                            </form>
                        </div>
                    </div>
                    <div class="panel-group" id="accordion">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne">
                                        Добавить материал
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseOne" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <form method="post" action="new-materials" enctype="multipart/form-data" onsubmit="return CheckForm_isFilled_path_or_saveFile(this);">

                                        <input type="hidden" name="id_digest" value="<?= $digest->id_digest ?>">
                                        <div class="row">
                                            <div class="col-md-3">
                                                <select id="type" name="type" class="form-control font_opensans select_form2">
                                                    <option value="">Тип работы</option>
                                                    <option value="video">Видео</option>
                                                    <option value="image">картинка</option>
                                                    <option value="file">файл</option>
                                                    <option value="pdf">презентация</option>
                                                </select>

                                                <textarea name="comment" placeholder="Комментарий" style="width: 100%; margin-top: 15px;" rows="3"></textarea>
                                            </div>

                                            <div class="col-md-7">
                                                <div id="file"></div>
                                            </div>
                                        </div>

                                    </form>
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <a data-toggle="collapse" data-parent="#accordion" href="#collapseTwo">
                                        Поменять обложку для работы
                                    </a>
                                </h4>
                            </div>
                            <div id="collapseTwo" class="panel-collapse collapse">
                                <div class="panel-body">

                                    <?php $form = ActiveForm::begin([
                                        'method' => 'post',
                                        'action' => 'replace-file',
                                        'options' => [
                                            'enctype' => 'multipart/form-data',
                                        ],
                                        'fieldConfig' => [
                                            'template' => "<div class='label_error' style='display: block;'> {error} </div><div> {input} </div>",
                                        ],
                                    ]); ?>

                                    <?= $form->field($ReplaceImage, 'file')->fileInput([]) ?>

                                    <?= Html::hiddenInput('id_digest', $digest->id_digest, ['id' => 'id_digest']) ?>

                                    <?= Html::submitInput('Поменять', [
                                        'class' => 'btn btn-primary'
                                    ]) ?>

                                    <?php ActiveForm::end(); ?>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="col-md-12">
        <div class="flex-container">
            <?php if(count($materials) >= 1){ ?>
                <?php foreach($materials as $val){ ?>

                    <div class="part edit" data-block-material-id="<?= $val->id_digest_materials ?>">

                        <div class="example2">
                            <?= $PrepareMaterials[$val->id_digest_materials] ?>
                        </div>

                        <br />

                        <div><strong>Тип: </strong><?= $val->type ?></div>

                        <div>
                            <strong>Описание: </strong>
                            <i data-material-this_active="true"
                                 data-material-id="<?= $val->id_digest_materials ?>"
                                 data-material-el="material_comment<?= $val->id_digest_materials ?>"
                                >
                                <?= $val->comment ?>
                                <i class="glyphicon glyphicon-pencil" aria-hidden="true"></i>
                            </i>
                        </div>

                        <div class="caption">
                            <a href="#" data-material-delete="<?= $val->id_digest_materials ?>" class="btn btn-danger">удалить</a>
                        </div>

                    </div>

                <?php } ?>
            <?php } ?>
        </div>
    </div>
</div>