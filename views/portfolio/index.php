<?php
/**
 * Created by PhpStorm.
 * User: kashtanov
 * Date: 05.04.2016
 * Time: 14:21
 */
use yii\helpers\Html;
use app\models\DigestModel;
?>
<h3>Работы студентов</h3>
<div class="row">
    <div class="col-sm-12 flex-container">
        <?php foreach($data as $id_stud => $val){ ?>
            <div class="part">
                <a href="/portfolio/details?id_digest=<?= $val['id_digest'] ?>">
                    <div class="thumb" style="background-image: url('<?= DigestModel::GetPathCity() . $val['id_stud'] . "/" . $val['path'] ?>')"></div>
                </a>
                <div class="caption">
                    <h4>
                        <a href="/portfolio/details?id_digest=<?= $val['id_digest'] ?>">
                            <?= $val['name'] ?>
                        </a>
                    </h4>
                    <p>Студент: <?= $val['fio_stud'] ?></p>
                    <p>Оценка: <?= $val['mark'] ?></p>
                </div>
            </div>
        <?php } ?>
    </div>
</div>
