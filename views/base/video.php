<?php
/**
 * Created by PhpStorm.
 * User: kashtanov
 * Date: 05.05.2016
 * Time: 12:28
 */

use yii\helpers\Html;

?>


<div class="row">
    <div class="col-md-3 label-div">
        <?= \Yii::t('js_superusser', 'video') ?>:
    </div>
    <div class="col-md-9">
        <?= Html::activeTextInput($model['digest'], 'path', [
            'placeholder'   => 'http:// [ссылка на видео]',
            'class'         => 'form-control font_opensans select_form2 inline_block',
        ]) ?>
    </div>
    <div class="col-md-12" style="margin-top: 15px;">
        <?= Html::submitInput('Отправить', ['class' => 'btn btn-success']); ?>
    </div>
</div>

