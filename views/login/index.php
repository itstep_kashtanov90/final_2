<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use yii\bootstrap\Alert;

if (Yii::$app->session->getFlash('error_login')) {
    echo Alert::widget(['options' => ['class' => 'alert-danger font_opensans font_size_14'], 'body' => \Yii::t('login_index','not_user')]);
}
if (Yii::$app->session->getFlash('restore_pass')) {
    echo Alert::widget(['options' => ['class' => 'alert-success font_opensans font_size_14'], 'body' => \Yii::t('login_index','mail_confirm')]);
}
if (Yii::$app->session->getFlash('restore_pass_ok')) {
    echo Alert::widget(['options' => ['class' => 'alert-success font_opensans font_size_14'], 'body' => \Yii::t('login_index','pass_changed')]);
}
if (Yii::$app->session->getFlash('error_email')) {
    echo Alert::widget(['options' => ['class' => 'alert-danger font_opensans font_size_14'], 'body' => \Yii::t('login_index','mail_not_fond')]);
}
if (Yii::$app->session->getFlash('not_city')) {
    echo Alert::widget(['options' => ['class' => 'alert-danger font_opensans font_size_14'], 'body' => \Yii::t('login_index','not_city')]);
}
?>



<div class="col">
    <h2>Portfolio</h2>
    <?php $form = ActiveForm::begin(['action' => Url::toRoute(['/login/']), 'options' => ['class' => 'form-signin']]); ?>
    <div class="login_form">
        <?= $form->field($model, 'list_city[]')->dropDownList($city_select, ['class'=>'city_id form-control'])->label(\Yii::t('login_index','city')); ?>
        <?= $form->field($model, 'username', [])->textInput(['value'=>$_SESSION['username']])->label(\Yii::t('login_index','login')) ?>
        <?= $form->field($model, 'password', [])->passwordInput()->label(\Yii::t('login_index','pass')) ?>
        <div class="form-group" align="left">
            <?= Html::submitButton(\Yii::t('login_index','enter'), ['class' => 'btn btn-success']) ?>
        </div>
    </div>
    <div><a href="login/restoreindex" id="restore" class="login_link"><?=\Yii::t('login_index','re_pass')?></a></div>
    <?php ActiveForm::end(); ?>
</div>

