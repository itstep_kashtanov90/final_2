<?php
use yii\helpers\Html;
use yii\widgets\ActiveForm;
use yii\helpers\Url;
use yii\bootstrap\Alert;

if (Yii::$app->session->getFlash('error_login')) {
    echo Alert::widget(['options' => ['class' => 'alert-danger font_opensans font_size_14'], 'body' => 'Такого пользователя не существует']);
}
if (Yii::$app->session->getFlash('restore_pass')) {
    echo Alert::widget(['options' => ['class' => 'alert-success font_opensans font_size_14'], 'body' => 'Письмо для подтверждения отправлено на почту']);
}
if (Yii::$app->session->getFlash('restore_pass_ok')) {
    echo Alert::widget(['options' => ['class' => 'alert-success font_opensans font_size_14'], 'body' => 'Пароль сменен']);
}
if (Yii::$app->session->getFlash('error_email')) {
    echo Alert::widget(['options' => ['class' => 'alert-danger font_opensans font_size_14'], 'body' => 'Пользователя с таким email не существует']);
}
if (Yii::$app->session->getFlash('not_city')) {
    echo Alert::widget(['options' => ['class' => 'alert-danger font_opensans font_size_14'], 'body' => 'Вы не выбрали город']);
}
?>

<div class="col">
    <h2>Восстановление доступа</h2>
    <form action="<?=Url::toRoute(['/login/restore/']);?>" method="POST" class="form-signin">
        <div class="login_form">
            <div class="form-group">
                <label><?=\yii::t('default','city')?></label>
                    <select  id="select_town" name="list_city" class="text-center font_opensans city_id form-control restore_pas" style="width: 100%;">
                        <?php
                        foreach($city_select as $key=> $city){
                            ?>
                            <option class="on_select_con" value="<?= $key?>" ><?= $city;?></option>
                        <? } ?>
                    </select>

            </div>
            <div class="form-group">
                <label for="restore_email">ваш e-mail</label>
                    <input id="restore_email" name="restore_email" class="form-control" type="email" required="required" style="width: 100%;">

            </div>
            <div class="form-group">
                <label for="restore_password">введите новый пароль</label>
                    <input  id="restore_password" name="restore_password" class="form-control" type="password" required="required" style="width: 100%;">

            </div>

            <?= Html::submitButton('Отправить', ['class' => 'btn btn-success']) ?>
            <?= Html::a( 'Назад', Url::toRoute(['/']),['class' => 'btn btn-default']); ?>
        </div>
    </form>
</div>
