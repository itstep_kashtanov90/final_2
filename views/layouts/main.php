<?php

use yii\helpers\Url;
use yii\helpers\Html;
use app\assets\AppAsset;

/* @var $this \yii\web\View */
/* @var $content string */

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?= Html::csrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <script src="//ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js" type="text/javascript"></script>
    <link href='http://fonts.googleapis.com/css?family=Open+Sans:400,600,300&subset=latin,cyrillic' rel='stylesheet' type='text/css'>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.2/js/bootstrap.min.js"></script>
    <link href="//netdna.bootstrapcdn.com/twitter-bootstrap/2.3.2/css/bootstrap-combined.min.css" rel="stylesheet">
    <link href="//netdna.bootstrapcdn.com/font-awesome/3.2.1/css/font-awesome.min.css" rel="stylesheet">


    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>
<div class="wrap_to_all"></div>
<div class="navbar navbar-fixed-top" role="navigation">
    <?php $this->beginContent('@app/views/layouts/header.php'); ?>
    <?php $this->endContent(); ?>
</div>
<div class="container-fluid">
    <?= $content ?>
</div>
<div class='ajax-loader-overlay' style="display: none;"></div>
<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
