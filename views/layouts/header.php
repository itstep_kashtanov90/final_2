<?php
use yii\helpers\Html;
use yii\helpers\Url;
?>

<?php $controller_name = Yii::$app->controller->id ?>
<?php $action_name =  Yii::$app->controller->action->id; ?>

<nav class="navbar navbar-default">
    <div class="container-fluid">
        <div class="collapse navbar-collapse">
            <ul class="nav navbar-nav">
                <? if ($controller_name == 'portfolio' && $action_name == 'index' || $action_name == 'details') { ?>
                <li class="active">
                    <a href="<?= Url::toRoute('/portfolio') ?>">
                        <span>Просмотр</span>
                    </a>
                </li>
                <? } else { ?>
                    <li>
                        <a href="<?= Url::toRoute('/portfolio') ?>">
                            <span>Просмотр</span>
                        </a>
                    </li>
                <? } ?>
                <? if ($action_name == 'load') { ?>
                <li class="active">
                    <a href="<?= Url::toRoute('/portfolio/load/') ?>">
                        <span>Загрузить</span>
                    </a>
                </li>
                <? } else { ?>
                <li>
                    <a href="<?= Url::toRoute('/portfolio/load/') ?>">
                        <span>Загрузить</span>
                    </a>
                </li>
                <? } ?>
            </ul>
            <a class="btn btn-danger btn-close" style="margin-top: 7px;" href="<?= Url::toRoute(['base/logout']) ?>">
                <?=\yii::t('layouts_header','exit')?>
            </a>
            <ul class="nav navbar-nav navbar-right">
                <li>
                    <div class="foto_holder">
                        <?if(!empty($_SESSION['photo_pas'])):?>
                            <?= Html::img( WEB_SERVER_PATH_STUD_UPLOAD_PHOTO.$_SESSION['photo_pas'], ['class' => 'img_header', 'style' => 'margin-top: 5px; height: 40px;']) ?>
                        <?else:?>
                            <?= Html::img('/public/image/no-avatar.svg', [
                                'class' => 'img_header_teach',
                                'style' => 'margin-top: 5px; height: 40px;'
                            ]) ?>
                        <?endif;?>
                    </div>
                </li>
                <li>
                </li>
            </ul>
        </div><!-- /.navbar-collapse -->
    </div><!-- /.container-fluid -->
</nav>