<?php

$params = require(__DIR__ . '/params.php');

$config = [
    'id' => 'basic',
    'basePath'      => dirname(__DIR__),
    'bootstrap'     => ['log'],
    'language'      => 'ru-RU',
    'defaultRoute' => 'portfolio',
    'components'    => [
        'urlManager'   => [
            'enablePrettyUrl' => true,
            'showScriptName'  => false,

            'rules'           => [
                # i18n
                //'<controller:\w+>/<action:\w+>/*'=>'<controller>/<action>',
                //'<action:en|ru|bg|pt|lt|ua>' => 'i18n/<action>',

                // ru => i18n/ru
                // en => i18n/en
                // bg => i18n/bg
                // pt => i18n/pt
            ],
        ],
        'i18n'         => [
            'translations' => [
                '*' => [
                    'class'          => 'yii\i18n\PhpMessageSource',
                    //'basePath' => '@app/messages',
                    'sourceLanguage' => '�p-CH',
                    'fileMap'        => [
                        // 'app'       => 'app.php',
                        //'app/error' => 'error.php',
                    ],
                ],
                'file-input*' => [
                    'class' => 'yii\i18n\PhpMessageSource',
                    'basePath' => dirname(__FILE__).'/../vendor/2amigos/yii2-file-input-widget/src/messages/',
                ],
            ],
        ],
        'request' => [
            // !!! insert a secret key in the following (if it is empty) - this is required by cookie validation
            //'cookieValidationKey' => '6Qwkg7MWyH8M4qVI2jWY0kPN3ZWspnfg',
            'cookieValidationKey'    => '123',
            'enableCookieValidation' => false,
            'enableCsrfValidation'   => false,
        ],
        'cache' => [
            'class' => 'yii\caching\FileCache',
        ],
//        'user'         => [
//            'identityClass'   => 'app\models\User',
//            'enableAutoLogin' => false,
//        ],
        'errorHandler' => [
            'errorAction' => 'login/error',
        ],
        'mailer' => [
            'class' => 'yii\swiftmailer\Mailer',
            // send all mails to a file by default. You have to set
            // 'useFileTransport' to false and configure a transport
            // for the mailer to send real emails.
            'useFileTransport' => true,
        ],
        'log' => [
            'traceLevel' => YII_DEBUG ? 3 : 0,
            'targets' => [
                [
                    'class' => 'yii\log\FileTarget',
                    'levels' => ['error', 'warning'],
                ],
            ],
        ],
        'db' => require(__DIR__ . '/db.php'),
    ],
    'params' => $params,
];

if (YII_ENV_DEV) {
    // configuration adjustments for 'dev' environment
    $config['bootstrap'][] = 'debug';
    $config['modules']['debug'] = [
        'class' => 'yii\debug\Module',
    ];

    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
    ];
}

return $config;
