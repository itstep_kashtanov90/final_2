<?
/*
 * Пути на сервере
*/


//Путь куда грузятся домашки преподователя
define('SERVER_PATH_TEACH_UPLOAD', '/var/www/new.logbook.itstep.org/web/upload_teach/');
//Путь куда грузятся домашки преподователя (web)
define('WEB_SERVER_PATH_TEACH_UPLOAD', 'http://new.logbook.itstep.org/upload_teach/');


/*
//Путь куда грузятся домашки преподователя
define('SERVER_PATH_TEACH_UPLOAD', '/var/www/logbook.itstep.org/new/logbook/uploader/uploads/');
//Путь куда грузятся домашки преподователя (web)
define('WEB_SERVER_PATH_TEACH_UPLOAD', 'http://logbook.itstep.org/new/logbook/uploader/uploads/');
*/

//Путь куда грузятся домашки из учебных материалов (REPOSITORY)
define('SERVER_PATH_REPOSITORY_UPLOAD', '/var/www/new.logbook.itstep.org/web/upload/');


//Путь куда грузят домашки студетнов
define('SERVER_PATH_STUD_UPLOAD', '/var/www/mystat.itstep.org/web/uploads/');
//Путь куда грузят домашки студетнов (web)
define('WEB_SERVER_PATH_STUD_UPLOAD', 'http://mystat.itstep.org/uploads/');


//Путь для фоток преподов (web)
define('WEB_SERVER_PATH_TEACH_UPLOAD_PHOTO', '/var/www/new.logbook.itstep.org/web/');
//Путь для фоток students
define('SERVER_PATH_STUD_UPLOAD_PHOTO', '/var/www/new.logbook.itstep.org/web/');
//Путь students photos (web)
define('WEB_SERVER_PATH_STUD_UPLOAD_PHOTO', 'http://new.logbook.itstep.org/');


/*
//Путь для фоток преподов (web)
define('WEB_SERVER_PATH_TEACH_UPLOAD_PHOTO', '/home/www/logbook.itstep.org/new/logbook/');
//Путь для фоток students
define('SERVER_PATH_STUD_UPLOAD_PHOTO', '/home/www/logbook.itstep.org/new/logbook/');
//Путь students photos (web)
define('WEB_SERVER_PATH_STUD_UPLOAD_PHOTO', 'http://logbook.itstep.org/new/logbook/');
*/
//--------------------------------------------------------------------------------------------------------------------------------//

/*
 * Локальные пути и данные
*/

//Путь куда грузят домашки студетнов
define('LOCAL_PATH_STUD_UPLOAD', '/var/www/mystat.itstep.org/web/uploads/');
//Путь куда грузят домашки студетнов (web)

//Путь куда грузяться домашки преподователя
define('LOCAL_PATH_TEACH_UPLOAD', 'upload_hw/');
//Путь куда грузяться домашки из учебных материалов (REPOSITORY)
define('LOCAL_PATH_REPOSITORY_UPLOAD', 'upload/');
define('LOCAL_PATH_UPLOAD', 'C:/WEB/OpenServer/domains/itstep_logbook/web/');
// Дата для работы локальной базы
define('DATE_LOCAL', date("Y-m-d"));

defined('LOGBOOK_PUBLIC') or define('LOGBOOK_PUBLIC', '`logbook_public`.');

define('CONTROL_WORK_MARK', 'mark2');
define('CLASS_WORK_MARK', 'mark4');
define('NO_DATA_FOR_OUTPUT', 'Нет данных для отображения');

