<?php

    return [
        'teach_main'    => 'Основной преподаватель',
        'request_of_date'    => 'Запросить дату',
        'teach_replace' => 'Преподаватель на замене',
        'material_add'  => 'Добавить материал',
        'homework'      => 'ДОМАШНЕЕ ЗАДАНИЕ',
        'labwork'       => 'ЛАБОРАТОРНАЯ РАБОТА',
        'copy_data'     => 'Скопировать данные с предыдущей пары',
        'login_mystat'  => 'Заходил в Mystat',
        'cancel_save'   => "Отметить и сохранить статус 'был' для всех студентов в группе",
        'mark_all'      => 'Отметить всех',
        'visit'         => 'Посещение',
        'test'          => 'Контрольная работа',
        'classwork'     => 'Работа в классе',
        'award'         => 'Награжден',
        'comment'       => 'Комментарий',
        'to_unlock'     => 'Для разблокировки введите тему урока и отметьте радио кнопку',
        'material_on'   => 'Добавить материал на группу',
        'select'        => 'Выберите тип',
        'char_left'     => 'Осталось символов',
        'attention'     => 'Обратите Внимание!',
        'theme'         => 'Тема',
        'not_empty'     => 'это поле не может быть пустым',
        'description'   => 'Описание',
        'ref_paste'     => 'Вставьте ссылку',
        'file_download' => 'При загруженном файле ссылку добавить нельзя, кроме как обложку для книги',
        'file_load'     => 'Загрузите файл',
        'ref_if'        => 'Если есть ссылка, то это поле активно только для загрузки обложки для книги',
        'button_after'  => 'Эта кнопка станет активной после заполнения обязательных полей',
        'add'           => 'Добавить',
        'home_load'     => 'Загрузка домашнего задания',
        'recommended'   => 'Рекомендованное задание от учебной части',
        'home_select'   => '---Выбор домашнего задания---',
        'home_exe'      => 'Срок выполнения ДЗ студентами до',
        'review'        => 'Обзор',
        'theme_less'    => 'Тема урока',
        'lab_select'    => '---Выбор лабораторной работы---',
        'select_type'   => 'Выберите тип',
    ];