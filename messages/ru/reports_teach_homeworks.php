<?php

return [
    'month'     => 'Месяц',
    'week'      => 'Неделя',
    'day'       => 'День',
    'pair'      => 'кол-во пар',
    'assigned'  => 'задано',
    'received'  => 'получено',
    'reviewed'  => 'проверено'
];