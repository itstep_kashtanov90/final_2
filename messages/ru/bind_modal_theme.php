<?php

return [
    'title'     => 'Описание недели',
    'more_than' => 'Описание недели не должно превышать 1000 символов',
    'left'      => 'Осталось символов'
];