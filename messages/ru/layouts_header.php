<?php

return [
    'profile'       => 'Мой профиль',
    'pass_change'   => 'Смена пароля',
    'exit'          => 'Выйти',
    'pass_enter'    => 'Введите новый пароль',
    'pass_new'      => 'Новый пароль',
    'pass_retype'   => 'Повторите пароль',
    'edit'          => 'Изменить'
];