<?php

return [
    'absent'            => 'Н',
    'passed'            => 'Зачет',
    'failed'            => 'Незачет',
    'unsatisfactory'    => 'Неудовлетворительно',
    'satisfactory'      => 'Удовлетворительно',
    'good'              => 'Хорошо',
    'excellent'         => 'Отлично',
    'denial'            => 'Недопуск',
    'permission'        => 'Допуск'
];