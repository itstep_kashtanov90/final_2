<?php

return [
    'this_task'     => 'У этой задачи еще нет комментариев',
    'failed_save'   => 'Не удалось сохранить или загрузить комментарии:',
    'del'           => 'Удалить',
    'View'          => 'Просмотреть',
    'Edit'          => 'Редактировать',
    'Confirm'       => 'Редактировать',
];