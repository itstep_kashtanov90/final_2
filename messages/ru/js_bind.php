<?php

return [
    'up_home'           => 'Загрузка Д\З',
    'up_lesson'         => 'Загрузка Уроков',
    'up_labor'          => 'Загрузка Лабораторных',
    'up_books'          => 'Загрузка Книг',
    'up_video'          => 'Загрузка Видео',
    'up_present'        => 'Загрузка Презентаций',
    'up_tests'          => 'Загрузка Тестов',
    'up_articles'       => 'Загрузка Статей',
    'sel_test'          => 'Выберите тест для привязки',
    'add_test'          => 'Добавить тест',
    'was_test'          => 'Тест уже был привязан',
    'del_subject'       => 'Удалить предмет',
    'del_material'      => 'Удалить привязанный материал?',
    'sel_subject'       => 'Выбор предмета',
    'all_forms'         => 'Выбор предмета',
    'copy'              => 'копия',
    'do_you_want'       => 'Вы хотите скопировать все привязанные матод. материалы в новый предмет',
    'to_choose'         => 'Необходимо выбрать форму обучения и предмет.',
    'no_materials'      => 'Нет материалов',
    'attention'         => 'Обратите внимание!',
    'order_video_1'     => 'Для корректной загрузки видеоматериала необходимо вставить ссылку на видео из адресной строки.',
    'order_video_2'     => 'необходимо скопировать ссылку под надписью "WordPress Shortcode',
    'ref'               => 'Ссылка',
    'ref_preview'       => 'Ссылка на просмотр:',
    'sel_cover'         => 'Выберите обложку для книги:',
    'book_cover'        => 'Обложка книги',
    'test_name'         => 'Название теста:',
    'no_subjects'       => 'Нет предметов',
    'fill_fields'       => 'Заполните поля для добавления нового предмета!',
    'more_than_1'       => 'Ваше сообщение превышает',
    'more_than_2'       => 'символов и оно автоматически будет обрезано',
    'copy_method'       => 'Вы уверены, что хотите запросить копию методпакета для формы обучения',
    'sent_complete'     => 'Заявка отправлена успешно',
    'not_sent'          => 'Заявка не отправлена, произошла ошибка',
    'are_you_sure'      => 'Вы уверены? Этот материал будет недоступен в будущем для добавления!'
];