<?php

    return [
        'sel_direction' => 'Выбор направления',
        'sel_stream'    => 'Выбор потока',
        'sel_subject'   => 'Выбор предмета',
        'sel_package'   => 'Выбор пакета',
        'bind'          => 'Привязать',
        'are_you_sure'  => 'Вы уверены что хотите удалить привязку?',
        'remove'        => 'Удалить',
    ];