<?php

return [
    'from_city'     => 'из города',
    'copy_method'   => 'просит Вас скопировать методпакет',
    'for_form'      => 'для формы обучения',
    'for_feedback'  => 'Для обратной связи можете использовать e-mail',
    'or_phone'      => 'или номер телефона',
    'request_copy'  => 'Запрос на копирование методпакета',
    'sel_type'      => 'Выберите тип',
    'sel_all'       => 'Выбрать все',
    'all_city'      => 'Все города'
];