<?php

return [
    'sel_file'      => 'Выберите файл',
    'review'        => 'Обзор…',
    'select'        => 'Выберите…',
    'no_matches'    => 'Совпадений не найдено',
    'search'        => 'Поиск…',
];