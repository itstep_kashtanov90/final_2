<?php

return [
    'not_come'  => 'Не приходил',
    'not_phone' => 'Телефон не указан',
    'not_email' => 'Почта не указана',
    'year'      => 'года',
    'years'     => 'лет'
];