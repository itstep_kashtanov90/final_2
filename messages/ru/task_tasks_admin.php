<?php

    return [
        'data_start'     => 'Дата начала',
        'data_end'       => 'Дата окончания',
        'control'        => 'Управление',
        'not_assigned'   => 'Не назначен',
        'confirm'        => 'Подтвердить выполнение',
        'return_to_work' => 'Вернуть в работу',
        'task'           => 'Задание',
    ];