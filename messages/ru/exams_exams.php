<?php

return [
    'exam'          => 'Экзамен',
    'permission_w'  => 'с допуском',
    'permission_o'  => 'без допуска',
    'date_end'      => 'Дата окончания',
    'not_specified' => 'неуказана',
    'form'          => 'Форма выставления оценок',
    'permission'    => 'Допуск',
    'mark'          => 'Оценка',
    'full_name'     => 'ФИО студента',
];