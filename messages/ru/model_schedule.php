<?php

return [
    'schedule'  => 'Id Расписания',
    'date'      => 'Дата',
    'teach'     => 'Id Преподавателя',
    'feed'      => 'Лента',
    'class'     => 'Id Потока',
    'group'     => 'Id Группы',
    'specialty' => 'Id Специальности',
    'classroom' => 'Id Аудитории',
    'active'    => 'Активен'
];