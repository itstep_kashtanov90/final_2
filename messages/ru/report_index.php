<?php

return [
    'report_hours'  => 'Отчёт по отчитанным часам',
    'form_report'   => 'Формы сдачи отчётов',
    'report_cons'   => 'Отчёты по консультациям',
    'report_sal'    => 'Отчёты по ЗП'
];