<?php

return [
    "theme"         => "Тема работы",
    "file_teach"    => "Файл преподавателя",
    "date"          => "Дата выдачи",
    "pair"          => "пара",
    "file_stud"     => "Файл от студента",
    "date_load"     => "Дата загрузки",
];