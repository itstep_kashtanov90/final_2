<?php

return [
    'not_user'      => 'Такого пользователя не существует',
    'mail_confirm'  => 'Письмо для подтверждения отправлено на почту',
    'pass_change'   => 'Пароль сменен',
    'mail_not_fond' => 'Пользователя с таким email не существует',
    'not_city'      => 'Вы не выбрали город',
    'pass_restore'  => 'Восстановление пароля',
    'city'          => 'город',
    'pass'          => 'пароль',
    'send'          => 'Отправить',
    'back'          => 'Назад'
];