<?php

return [
    'direction' => 'Направление',
    'stream'    => 'Поток',
    'subject'   => 'Предмет',
    'package'   => 'Пакет',
    're_bind'   => 'Удалить привязку'
];