<?php

return [
    'shows'     => 'В отчете отображается кол-во пропусков студентом за период: день, неделя, месяц.',
    'if_stud'   => 'Если студент хоть раз пропустил в этом месяце - он попадет в отчет.',
    'week_7'    => 'Неделя считается от сегодняшней даты минус 7 дней,  месяц считается как 30 дней в зависимости от текущей даты: например, с 15-го апреля  по 15 мая.',
    'stream'    => 'Поток',
    'skipped'   => 'Пропущено',
    'today'     => 'Сегодня',
    'week'      => 'Неделя',
    'month'     => 'Месяц'
];