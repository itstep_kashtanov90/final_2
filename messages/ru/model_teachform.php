<?php

return [
    'full_name'     => 'ФИО',
    'address'       => 'Адрес',
    'home_phone'    => 'Домашний телефон',
    'mobile'        => 'Мобильный телефон',
    'Internal'      => 'Внутренний телефон',
    'corporate'     => 'Корпоративный Email',
    'birth'         => 'Дата рождения',
    'position'      => 'Должность',
    'sel_file'      => 'Выберите файл',
];