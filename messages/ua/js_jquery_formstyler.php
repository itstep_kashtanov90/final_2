﻿<?php

return [
    'sel_file'      => 'Виберіть файл',
    'review'        => 'Огляд…',
    'select'        => 'Виберіть…',
    'no_matches'    => 'Збігів не знайдено',
    'search'        => 'Пошук…',
];