﻿<?php

return [
    'schedule'  => 'Id розкладу',
    'date'      => 'Дата',
    'teach'     => 'Id викладача',
    'feed'      => 'Пара',
    'class'     => 'Id потоку',
    'group'     => 'Id групи',
    'specialty' => 'Id спеціальності',
    'classroom' => 'Id аудиторії',
    'active'    => 'Активний'
];