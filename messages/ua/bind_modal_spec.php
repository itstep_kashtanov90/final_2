﻿<?php

return [
    'sub_edit'      => 'Редагування предмету',
    'name'          => 'Назва предмету',
    'short_name'    => 'Скорочена назва предмету',
    'method_del'    => 'Видалити методпакет'
];