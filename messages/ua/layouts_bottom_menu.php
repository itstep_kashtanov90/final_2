﻿<?php

    return [
        'registration' => 'Час реєстрації',
        'attendees'    => 'Присутні',
        'timetable'    => 'Розклад',
        'announ'       => 'Оголошення',
        'groups'       => 'Відвідуваність груп',
        'materials'    => 'Навчальні матеріали',
        'homework'     => 'Домашні завдання',
        'classwork'    => 'Робота в класі',
        'exams'        => 'Мої іспити',
        'reports'      => 'Звіти',
        'tasks'        => 'Завдання',
        'stud'         => 'Студенти',
    ];
