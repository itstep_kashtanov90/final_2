﻿<?php

return [
    'report_hours'  => 'Звіт з відпрацьованих годин',
    'form_report'   => 'Форми подання звітів',
    'report_cons'   => 'Звіти з консультацій',
    'report_sal'    => 'Звіти з ЗП'
];