﻿<?php

return [
    'exam'          => 'Іспит',
    'permission_w'  => 'з допуском',
    'permission_o'  => 'без допуску',
    'date_end'      => 'Дата закінчення',
    'not_specified' => 'не зазначено',
    'form'          => 'Форма виставлення оцінок',
    'permission'    => 'Допуск',
    'mark'          => 'Оцінка',
    'full_name'     => 'ПІБ студента',
];