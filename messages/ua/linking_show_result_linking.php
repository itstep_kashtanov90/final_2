﻿<?php

return [
    'direction' => 'Напрямок',
    'stream'    => 'Потік',
    'subject'   => 'Предмет',
    'package'   => 'Пакет',
    're_bind'   => 'Видалити прив\'язку'
];