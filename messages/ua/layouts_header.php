﻿<?php

return [
    'profile'       => 'Мій профіль',
    'pass_change'   => 'Зміна пароля',
    'exit'          => 'Вийти',
    'pass_enter'    => 'Введіть новий пароль',
    'pass_new'      => 'Новий пароль',
    'pass_retype'   => 'Повторіть пароль',
    'edit'          => 'Змінити'
];