﻿<?php

return [
    'month'     => 'Місяць',
    'week'      => 'Тиждень',
    'day'       => 'День',
    'pair'      => 'кількість пар',
    'assigned'  => 'задано',
    'received'  => 'отримано',
    'reviewed'  => 'перевірено'
];