﻿<?php

return [
    'absent'            => 'Н',
    'passed'            => 'Зараховано',
    'failed'            => 'Не зараховано',
    'unsatisfactory'    => 'Незадовільно',
    'satisfactory'      => 'Задовільно',
    'good'              => 'Добре',
    'excellent'         => 'Відмінно',
    'denial'            => 'Недопуск',
    'permission'        => 'Допуск'
];