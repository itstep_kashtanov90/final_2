﻿<?php

return [
    'full_name'     => 'ПІБ',
    'address'       => 'Адрес',
    'home_phone'    => 'Домашній телефон',
    'mobile'        => 'Мобільний телефон',
    'Internal'      => 'Внутрішній телефон',
    'corporate'     => 'Корпоративний е-mail',
    'birth'         => 'Дата народження',
    'position'      => 'Посада',
    'sel_file'      => 'Виберіть файл',
];