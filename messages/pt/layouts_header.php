<?php

return [
    'profile'       => 'Meu perfil',
    'pass_change'   => 'Alteração da senha',
    'exit'          => 'Sair',
    'pass_enter'    => 'Digite a nova senha',
    'pass_new'      => 'Nova senha',
    'pass_retype'   => 'Repita a senha',
    'edit'          => 'Alterar'
];