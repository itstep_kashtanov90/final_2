<?php

return [
    're_pass'       => 'recuparação de senha',
    'your_pass'     => 'sua senha',
    'confirm'       => 'para confirmação acesso o link',
    'confirm_pass'  => 'confirmação de senha',
];