<?php

return [
    'this_task'     => 'Esta tarefa ainda não tem comentários',
    'failed_save'   => 'Não foi possível salvar ou carregar comentários:',
    'del'           => 'Deletar',
    'View'          => 'Visualizar',
    'Edit'          => 'Editar',
    'Confirm'       => 'Confirmar',
];