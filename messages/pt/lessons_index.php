<?php

    return [
        'pair'  => 'a aula',
        'break' => 'Agora é o intervalo, ou você não tem a aula solicitada',
        'mark'  => 'Marcar em outra data',
    ];