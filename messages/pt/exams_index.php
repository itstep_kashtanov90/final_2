<?php

    return [
        'exams'     => 'Prova',
        'start'     => 'Início',
        'end'       => 'Final',
        'new'       => 'novo',
        'group'     => 'Turma',
        'subject'   => 'Disciplina',
        'not_found' => "Você não possui provas no momento",
    ];