<?php

return [
    'absent'            => 'H',
    'passed'            => 'Aprovado',
    'failed'            => 'Reprovado',
    'unsatisfactory'    => 'Insuficiente',
    'satisfactory'      => 'Suficiente',
    'good'              => 'Bom',
    'excellent'         => 'Excelente',
    'denial'            => 'Sem admissão',
    'permission'        => 'Admissão',
];