<?php

    return [
        'sel_direction' => 'Selecionar área',
        'sel_stream'    => 'Selecionar as turmas com aulas conjuntas',
        'sel_subject'   => 'Selecionar disciplina',
        'sel_package'   => 'Selecionar pacote',
        'bind'          => 'Associar',
        'are_you_sure'  => 'Tem certeza de que deseja excluir a associação?',
        'remove'        => 'Excluir',
    ];