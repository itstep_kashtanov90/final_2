<?php

return [
    'direction' => 'Área',
    'stream'    => 'Turmas com aulas conjuntas',
    'subject'   => 'Disciplina',
    'package'   => 'Pacote',
    're_bind'   => 'Excluir a associação'
];