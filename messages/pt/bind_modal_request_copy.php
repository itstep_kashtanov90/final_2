<?php

return [
    'method_copy'   => 'Solicitação de copiar do pacote de materiais didáticos',
    'form_sel'      => 'Selecione a forma de ensino, para a qual você deseja criar uma cópia do pacote de materiais didáticos',
];