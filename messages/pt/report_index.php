<?php

return [
    'report_hours'  => 'Relatório de horas lecionadas',
    'form_report'   => 'Formas de entrega de relatórios',
    'report_cons'   => 'Relatórios das consultas',
    'report_sal'    => 'Relatórios dos salários',
];