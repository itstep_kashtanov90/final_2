<?php

return [
    'report_groups' => 'Relatório de preenchimento de turma por',
    'report_room'   => 'Relatório de ocupação de salas de aula de _ a _',
    'report_stud'   => 'Relatório de uso de Mystat por estudantes',
];