<?php

return [
    'from_city'     => 'de cidade',
    'copy_method'   => 'pede para copiar o pacote de materiais didáticos',
    'for_form'      => 'para a forma de ensino',
    'for_feedback'  => 'Para feedback pode usar o e-mail',
    'or_phone'      => 'ou número de telefone',
    'request_copy'  => 'Solicitação de cópia de pacote de materiais didáticos',
    'sel_type'      => 'Selecione tipo',
    'sel_all'       => 'Selecione tudo',
    'all_city'      => 'Todas as cidades',
];