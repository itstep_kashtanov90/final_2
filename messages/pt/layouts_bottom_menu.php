<?php

    return [
        'registration' => 'Tempo de registro:',
        'attendees'    => 'Presentes',
        'timetable'    => 'Horário',
        'announ'       => 'Avisos',
        'groups'       => 'Frequência de turmas',
        'materials'    => 'Materiais didáticos',
        'homework'     => 'Homework assignments',
        'classwork'    => 'Trabalho na sala de aula',
        'exams'        => 'Minhas provas',
        'reports'      => 'Relatórios',
        'tasks'        => 'Tarefas',
        'stud'         => 'Estudantes',
    ];
