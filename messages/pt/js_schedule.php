<?php

return [
    're_timetable'      => 'O horário foi excluído.',
    'not_save'          => 'O horário não foi salvo. Foi incorretamente definido o grupo ou a profissão.',
    'no_stream'         => 'Não há turmas com aulas conjuntas',
    'no_subject'        => 'Não há disciplinas',
    'no_group'          => 'Não há grupos',

    'no_room'           => 'Não há salas de aula',
    'schedule_suc'      => 'O horário foi duplicado com sucesso',
    'schedule_dup'      => 'O horário foi duplicado com erros',
    'schedule_already'  => 'Para este professor já existe horário de aulas:',
    'pair'              => 'aulas em',

    'rewrite'           => 'Reescrever?',
    'schedule_save'     => 'O novo horário foi salvo.',
    'schedule_not_save' => 'O horário não foi salvo. Você não escolheu nenhuma aula, ou ocorreu um erro.',
    'sel_group twice'   => "Você selecionou o mesmo grupo duas vezes, por favor, exclua o campo desnecessário com o botão 'X' ou selecione outro grupoу",
    'group'             => 'Grupo',

    'no_one_sel'        => 'Não há nenhuma opção selecionada',
    'date_finich'       => 'Data de fim deve ser maior que a data de início',
];