<?php

return [
    'month'     => 'Mês',
    'week'      => 'Semana',
    'day'       => 'Dia',
    'pair'      => 'número de aulas',
    'assigned'  => 'entregue',
    'received'  => 'recebido',
    'reviewed'  => 'verificado',
];