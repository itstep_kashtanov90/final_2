<?php

    return [
        'all'      => 'TODOS',
        'generate' => 'Formar',
        'date'     => 'Data',
        'subject'  => 'Disciplina',
        'hours'    => 'Horas',
        'total'    => 'Em total',
        'not_data' => 'Não há dados por este período',
    ];