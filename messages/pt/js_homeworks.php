<?php

return [
    'more_than'     => 'Sua mensagem excede caracteres e será automaticamente cortada',
    'left'          => 'Carateres restantes:',
    'write'         => 'Deixar um comentário',
    'are_you_sure'  => 'Tem certeza de que deseja excluir o dever de casa?',
    'procedure'     => 'Este procedimento excluirá irreversivelmente seu arquivo e todos os deveres de casa dos estudantes a ele relacionados!',
    'sel_subject'   => 'Selecionar disciplina',
    're_file'       => 'Tem certeza de que deseja excluir este arquivo?',
    'An_error'      => 'Ocorreu um erro, provavelmente o arquivo do estudante não existe no servidor',
];