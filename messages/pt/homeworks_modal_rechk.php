<?php

return [
    "theme"         => "Tema de trabalho:",
    "file_teach"    => "Arquivo de professor:",
    "date"          => "Data de entrega:",
    "pair"          => "aula",
    "file_stud"     => "Arquivo de estudante:",
    "date_load"     => "Data de download:",
];