<?php

return [
    'field_login'   => 'Preencha o campo Login',
    'field_pass'    => 'Preencha o campo Senha',
    'login'         => 'Login',
    'pass'          => 'Senha',
    'field'         => 'Lembrar',
    'sel_city'      => 'Selecione cidade',
];