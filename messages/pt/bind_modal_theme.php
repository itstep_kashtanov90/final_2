<?php

return [
    'title'     => 'Descrição de semana',
    'more_than' => 'A descrição da semana não deve exceder 1000 caracteres',
    'left'      => 'Carateres restantes:',
];