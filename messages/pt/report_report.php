<?php

return [
    'from'          => 'Período de:',
    'to'            => 'a:',
    'generate'      => 'Formar',
    'hours_past'    => 'HORAS LECIONADAS',
    'subject'       => 'Disciplina',
    'hours'         => 'Horas',
    'pairs'         => 'Total de horas lecionadas:',
];