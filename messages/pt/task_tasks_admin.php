<?php

    return [
        'data_start'     => 'Data de início',
        'data_end'       => 'Data de termino',
        'control'        => 'Controle',
        'not_assigned'   => 'Não atribuído',
        'confirm'        => 'Confirmar a execução',
        'return_to_work' => 'Devolver para melhorar',
        'task'           => 'tarefa',
    ];