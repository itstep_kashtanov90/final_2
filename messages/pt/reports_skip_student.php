<?php

return [
    'shows'     => 'O relatório mostra o número de faltas de estudante por período: de um dia, de um ano, de um mês.',
    'if_stud'   => 'Se estudante tiver pelo menos uma falta neste mês, ele deverá entregar o relatório.',
    'week_7'    => 'Uma semana é calculada a contar da data de hoje menos de 7 dias, um mês é calculado a contar da data de hoje menos 30 dias: por exemplo, de 15 de Abril a 15 de Maio.',
    'stream'    => 'Turmas com aulas conjuntas',
    'skipped'   => 'Faltou ',
    'today'     => 'Hoje',
    'week'      => 'Semana',
    'month'     => 'Mês',
];