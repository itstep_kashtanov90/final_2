<?php

return [
    'full_name'     => 'Nome completo',
    'address'       => 'Endereço',
    'home_phone'    => 'Telefone fixo de casa',
    'mobile'        => 'Telefone celular',
    'Internal'      => 'Telefone fixo',
    'corporate'     => 'E-mail profissional',
    'birth'         => 'Data de nascimento',
    'position'      => 'Cargo',
    'sel_file'      => 'Selecione arquivo',
];