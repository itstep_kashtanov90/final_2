<?php

return [
    'att'       => 'Frequência',
    'dollars'   => 'Dólares',
    'for_exp'   => 'Por sucessos no estudo',
    'crystals'  => 'Cristais',
    'for_att'   => 'Por frequência ',
    'rewards'   => 'Prêmios',
    'single'    => 'São dados só uma vez',
    'perf'      => 'Aproveitamento:',
    'atts'      => 'Frequência:',
    'spec'      => 'Especialização:',
    'birth'     => 'Data de nascimento:',
    'mail'      => 'Email de um dos pais:',
    'networks'  => 'Redes sociais:',
    'leave'     => 'DEIXAR COMENTÁRIO',
    'phone'     => 'Telefone:',
    'address'   => 'Morada:',
    'flow'      => 'Turmas com aulas conjuntas',
];