<?php

return [
    'sel_subject'   => 'Selecionar disciplina',
    'sel_package'   => 'Selecionar pacote',
    'sel_stream'    => 'Selecionar turmas com aulas conjuntas',
    'success'       => 'A associação foi realizada com sucesso',
    'binding'       => 'A associação não existe mais',
];