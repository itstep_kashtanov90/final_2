<?php

return [
    'not_user'      => 'Este usuário não existe',
    'mail_confirm'  => 'A confirmação é enviada para o e-mail',
    'pass_change'   => 'A senha foi alterada',
    'mail_not_fond' => 'O usuário com este email não existe',
    'not_city'      => 'Você não selecionou a cidade',
    'pass_restore'  => 'Recuperação da senha',
    'city'          => 'cidade',
    'pass'          => 'senha',
    'send'          => 'Enviar',
    'back'          => 'Voltar',
];