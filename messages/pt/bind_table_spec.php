<?php

return [
    'work'      => 'Dever de casa',
    'less'      => 'Lições',
    'labs'      => 'Trabalhos laboratoriais',
    'books'     => 'Livros',
    'videos'    => 'Vídeo',
    'present'   => 'Apresentações',
    'tests'     => 'Testes',
    'articles'  => 'Artigos',
    'week_add'  => 'Adicionar semana',
];