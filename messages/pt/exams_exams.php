<?php

return [
    'exam'          => 'Provas:',
    'permission_w'  => 'com admissão',
    'permission_o'  => 'sem admissão',
    'date_end'      => 'Data final',
    'not_specified' => 'não especificada',
    'form'          => 'Método de avaliação',
    'permission'    => 'Admissão',
    'mark'          => 'Nota',
    'full_name'     => 'ФИО студента',
];