<?php

return [
    'planning'  => 'Criar horário de aulas',
    'remove'    => 'Excluir do horário',
    'planning'  => 'Criar horário de aulas',
    'Save'      => 'Salvar este horário para todas as turmas com aulas conjuntas',
    'Duplicate' => 'Duplicar horário de aulas',
    'button'    => 'Este botão irá salvar o estado atual do horário',
    'remove'    => 'REMOVER DO HORÁRIO DE AULAS',
    'schedule'  => 'Horário sobreposto',
    'inactive'  => 'Horário sobreposto não ativo',
];