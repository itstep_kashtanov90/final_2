<?php

return [
    'report'        => 'RELATÓRIO DE USO DE Mystat POR ESTUDANTES',
    'city'          => 'Cidade',
    'active_users'  => 'Número total de usuários ativos',
    'all_time'      => 'Visitaram durante todo o tempo',
    'all_week'      => 'Visitaram durante uma semana',
];