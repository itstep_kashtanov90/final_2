<?php

return [
    'task_complet'      => 'A execução da tarefa foi confirmada',
    'task_sent_back'    => 'A tarefa devolvida para melhoria',
    'enter_all'         => 'Por favor, preencha todos os dados!',
    'not_empty'         => 'Não se pode adicionar um comentário vazio',
];