<?php

    return [
        'stud'       => 'Estudantes',
        'feedbacks'  => 'Comentários sobre estudantes',
        'feedbacks_' => 'Comentários',
        'mailing'    => 'Mailing',
        'all_groups' => 'Todas as turma',
        'not_groups' => 'Você não tem uma turma',
        'was'        => 'Foi:',
        'was_s'      => 'Foi:',
        'last_visit' => 'Última visita:',
        'age'        => 'Idade:',
        'middle'     => 'Nota média:',
        'attendance' => 'Frequência:',
        'group'      => 'Turmas',
        'text'       => 'Texto do comentário',
        'send'       => 'Enviar',
        'too_short'  => 'A mensagem é curta demais!',
        'not_stud'   => 'Não há estudantes',
        'mail_success' => 'Newsletter de sucesso',
    ];