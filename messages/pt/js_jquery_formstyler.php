<?php

return [
    'sel_file'      => 'Selecione um arquivo',
    'review'        => 'Visualizar...',
    'select'        => 'Selecione...',
    'no_matches'    => 'Совпадений не найдено',
    'search'        => 'pesquisa',
];