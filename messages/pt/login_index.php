<?php

return [
    'not_user'      => 'Este usuário não existe',
    'mail_confirm'  => 'A confirmação é enviada para o e-mail',
    'pass_changed'  => 'A senha foi alterada',
    'mail_not_fond' => 'O usuário com este email não existe',
    'not_city'      => 'Você não selecionou a cidade',
    'login_log'     => 'Acesso ao registo eletrônico',
    'city'          => 'cidade',
    'login'         => 'login',
    'pass'          => 'senha',
    'enter'         => 'entrar',
    'manual'        => 'Instrução pelo sistema Logbook',
    're_pass'       => 'Recuperar a senha',
];