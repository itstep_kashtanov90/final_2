<?php

return [
    'teach' => 'Professor',
    'phone' => 'Telefone',
    'stud'  => 'Estudante',
    'group' => 'Turma',
    'days'  => 'Dias de atraso',
];