<?php

return [
    'no_sel_homework'   => 'Você não selecionou ou não baixou o dever de casa!',
    'already_dow'       => 'O arquivo foi baixado com sucesso',
    'no_sel_laboratory' => 'Você não selecionou ou não baixou o trabalho de laboratório!',
    're_uploaded'       => 'Você está prestes a excluir o dever de casa baixado!',
    're_all'            => 'Todos os deveres de casa associados aos estudantes serão excluídos.',

    're'                => 'Excluir?',
    'theme'             => 'Tema da lição',
    'no_theme'          => 'Não foi possível salvar o tema',
    'at_first'          => 'Em primeiro lugar deve-se especificar se você é o professor principal ou professor substituto',
    'in_unlock'         => 'Para desbloquear, digite a tema da lição e marque com botão rádio',

    'up_file'           => 'Pode-se baixar arquivo que não excede 100 Mb',
    'should_be_ref'     => 'E necessário ser um link para visualizar ou baixar o livro a partir de um recurso externo, você pode também baixar a capa do livro',
    'up_order_video'    => 'Para o download correto de material de vídeo, deve inserir o link para o vídeo a partir da barra de endereços',
    'up_order_present'  => "Para o download correto de apresentação, é necessário copiar o link abaixo da inscrição 'WordPress Shortcode'",
    'cover_ image'      => 'Para baixar as apresentações recomenda-se o uso do serviço: http://www.slideshare.net/',

    'uploading_1'       => "Depois de baixar a apresentação clique em 'Partilhar apresentação'.",
    'uploading_2'       => 'E copiar o código sob o campo WordPress Shortcode.',
    'dow_ref'           => 'Pode baixar o link para o artigo de um recurso externo',
    'cover_ image'      => 'A capa do livro deve conter uma imagem!',
    'big_size_file'     => 'O servidor não pode aceitar um arquivo tão grande.',

    'apologies'         => 'Por favor, aceite nossas sinceras desculpas...',
    'size_ image'       => 'O tamanho de tema e descrição deve ser superior a 3 caracteres',
    'sel_type'          => 'Selecione tipo',
    'are_you_sure '     => 'Tem certeza de que deseja excluir este material?',
    'material_not_re'   => 'O material não foi excluído ',
];