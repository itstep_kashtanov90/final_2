<?php

    return [
        "conf"            => "Confirmados",
        "form_all"        => "Todas as formas de ensino",
        "form_all_filter" => "Filtro por formas de ensino",
        "form_sel"        => "Selecione a forma de ensino",
        "materials"       => "Materiais dos professores",
        "methods"         => "Materiais didáticos",
        "method_add"      => "Atribuir material didático",
        "method_copy"     => "Cópia do pacote de materiais didáticos",
        "method_copy_up"  => "Solicitar a cópia do pacote de materiais didáticos",
        "new"             => "Novos",
        "short_name"      => "Nome abreviado",
        "sub_add"         => "Adicionar disciplina",
        "sub_edit"        => "Edição de disciplina",
        "sub_new"         => "Digite nova disciplina",
        "select"          => "Selecionar disciplina",
    ];