<?php

return [
    'colors'    => 'Descrição de cores',
    'form'      => 'Descrição das formas abreviadas',
    'orange'    => 'laranja',
    'not_noted' => 'o professor não assinalou os presentes',
    'red'       => 'vermelho',
    'less_7'    => 'o nível de frequência é inferior a 7 pessoas',
    'lilac'     => 'lilás',
    'less_70'   => 'o nível de frequência é inferior a 70%',
    'green'     => 'verde',
    'more_than' => 'A frequência é de 100%, apenas se o numero total de estudantes for superior a 7',
    'p'         => 'P - Educação Profissional de Computação',
    'j'         => 'J - Junior Academy',
    'e'         => 'E - Educação Europeia de Computação',
    'g'         => 'A - Cursos anuais',
    'classroom' => 'Sala de aula',
    'pairs'     => 'Aulas',
];