<?php

    return [
        'request'           => 'Entre com um motivo para o pedido',
        'date_change'       => 'Escolha uma data',
        'message'           => 'Você pode enviar um pedido para retornar para a data anterior , no máximo, 3 dias atrás',
        'cause'             => 'Entre com um motivo para o pedidos...',
    ];