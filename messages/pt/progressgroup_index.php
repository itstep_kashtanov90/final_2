<?php

return [
    'sel_group' => 'Selecionar turma',
    'sel_sub'   => 'Selecionar disciplina',
    'homework'  => 'Dever de casa',
    'tests'     => 'Testes',
    'classwork' => 'Trabalho na sala de aula',
    'shp'       => 'Falta',
    'late'      => 'Atraso',
    'comment'   => 'Comentário',
];