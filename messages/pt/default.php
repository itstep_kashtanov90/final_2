<?php

    return [
        "ok"                =>  "Ok",
        "save"              =>  "Salvar",
        "title"             =>  "Título",
        "send"              =>  "Enviar",
        "cancel"            =>  "Cancelar",
        "close"             =>  "Perto",



        "date"              =>  "Data",
        "topic"             =>  "Tópico",
        "city"              =>  "Cidade",
        "group"             =>  "Grupo",
        "subject"           =>  "Matéria",
        "stud"              =>  "Aluno",
        "stud_name"         =>  "Nome do aluno",
        "sub_sel"           =>  "Escolher matéria",


        "comment"           =>  "comentário",
        "comment_w"         =>  "escrever comentário",
        "date_load"         =>  "data de upload",
        "no_file"           =>  "pasta não existe",
        "not_found_file"    =>  "O arquivo não está disponível. Contate o seu administrador do servidor ou espera,
        caso não seja um Desenvolvedor.",
        "all_types"         => "todos os tipos",
        "not_admission"     =>  "não permitido",
        "load"              =>  "carregar trabalho do laboratório",
        "dead_line_for_lab" =>  "deadline para trabalho de laboratório",
        "theme_lab"         =>  "Assunto do laboratório",
        "text"              =>  "Mensagem de texto",
        "all_stud"          =>  "Todos os alunos",


        "mon"               =>  "Seg",
        "tue"               =>  "Ter",
        "wed"               =>  "Qua",
        "thu"               =>  "Qui",
        "fri"               =>  "Sex",
        "sat"               =>  "Sáb",
        "sun"               =>  "Dom",


        "mon_1"             => "SEG",
        "tue_1"             => "TER",
        "wed_1"             => "QUA",
        "thu_1"             => "QUI",
        "fri_1"             => "SEX",
        "sat_1"             => "SAB",
        "sun_1"             => "DOM",


        "monday"            =>  "Segunda-feira",
        "tuersday"          =>  "Terça-feira",
        "wednesday"         =>  "Quarta-feira",
        "thursday"          =>  "Quinta-feira",
        "friday"            =>  "Sexta-feira",
        "saturday"          =>  "Sábado",
        "sunday"            =>  "Domingo",


        "week"              =>  "Semana",
        "month"             =>  "Mês",
        "january"           =>  "Janeiro",
        "february"          =>  "Fevereiro",
        "march"             =>  "Março",
        "april"             =>  "Abril",
        "may"               =>  "Maio",
        "june"              =>  "Junho",
        "july"              =>  "Julho",
        "august"            =>  "Agosto",
        "september"         =>  "Setembro",
        "october"           =>  "Outubro",
        "november"          =>  "Novembro",
        "december"          =>  "Dezembro",


        "january_"          => "Janeiro",
        "february_"         => "Fevereiro",
        "march_"            => "Março",
        "april_"            => "Abril",
        "may_"              => "Maio",
        "june_"             => "Junho",
        "july_"             => "Julho",
        "august_"           => "Agosto",
        "september_"        => "Setembro",
        "october_"          => "Outubro",
        "november_"         => "Novembro",
        "december_"         => "Dezembro",

        "jan"               =>  "Jan",
        "feb"               =>  "Fev",
        "mar"               =>  "Mar",
        "apr"               =>  "Abr",
        "may__"             =>  "Mai",
        "jun"               =>  "Jun",
        "jul"               =>  "Jul",
        "aug"               =>  "Ago",
        "sep"               =>  "Set",
        "oct"               => "Out",
        "nov"               =>  "Nov",
        "dec"               =>  "Dez",

        /*   ключи (соответствуют имени префиксов городов в табличке public_city,
     в базе logbook_public) по которым переводятся названия городов на
     страничке Логина:                                                 */

        'lv'                => 'Lviv',
        'dp'                => 'Dnepropetrovsk',
        'lg'                => 'Lugansk',
        'kh'                => 'Kharkiv',
        'pl'                => 'Poltava',
        'ma'                => 'Mariupol',
        'od'                => 'Odessa',
        'dn'                => 'Donetsk',
        'kiev'              => 'Kyiv',
        'vn'                => 'Vinnytsia',
        'zp'                => 'Zaporizhzhia',
        'mk'                => 'Nikolaev',
        'rv'                => 'Rovno',
        'lt'                => 'Lutsk',
        'simf'              => 'Simferopol',
        'cher'              => 'Chernihiv',
        'minsk'             => 'Minsk',
        'ast'               => 'Astana',
        'gom'               => 'Gomel',
        'rdj'               => 'Rio de Janeiro',
        'tula'              => 'Tula',
        'lip'               => 'Lipetsk',
        'kish'              => 'Chisinau',
        'msk'               => 'Moscow',
        'bukh'              => 'Bucharest',
        'sof'               => 'Sofia',
        'vls'               => 'Vilnius',
        'tbi'               => 'Tbilisi',
        'alm'               => 'Almaty',



        'EXCLUDED'      =>  'Entrada proibida',
        'IS_NOT_PAID' =>  'Admissão à resolução de contabilidade',
        'ONE_TIME'      =>  'O acesso de uma só vez !',


    ];