<?php

return [
    'not_come'  => 'Não chegou',
    'not_phone' => 'Telefone não especificado',
    'not_email' => 'E-mail não especificado',
    'year'      => 'anos',
    'years'     => 'anos',
];