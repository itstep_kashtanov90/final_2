<?php

    return [
        'type'        => 'Tipo do material',
        'form'        => 'Forma de ensino',
        'material'    => 'Material',
        'full_name'   => 'Nome do professor',
        'advise'      => 'Recomendar',
        'no_found'    => 'Não há registros encontrados por estes critérios',
        'theme'       => 'Tema',
        'subject'     => 'Disciplina',
        'description' => 'Descrição',
        'city'        => 'Cidade',
        'date'        => 'Data',
    ];