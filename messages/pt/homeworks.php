<?php

return [
    'no_laboratory' => 'Não há trabalhos laboratoriais para este grupo',
    'no_homework'   => 'Não há deveres de casa para este grupo',
    'open_old_student' => 'Mostrar transferidos estudantes'
];