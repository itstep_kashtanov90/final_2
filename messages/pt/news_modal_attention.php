<?php

return [
    'attention' => 'Atenção!',
    'teach'     => 'Os professores são pessoalmente responsáveis pela admissão às aulas de estudantes sinalados como "não admitidos" ou que estão fora da lista de estudantes',
    'stud'      => 'Os estudantes com este status podem ser admitidos somente após a autorização por escrito do chefe de departamento de estudo ou contador.',
    'confirm'   => 'Confirmar',
];