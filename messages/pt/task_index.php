<?php

    return [
        'tasks_admin'   => 'Tarefas dos Administradores',
        'tasks_manager' => 'Tarefas do Gerente Administrativo',
        'tasks_add'     => 'Adicionar tarefa',
        'complete'      => 'Executar até...',
        'not_rooms'     => 'Não há salas de aula disponíveis',
        'tasks_create'  => 'Criar uma tarefa',
        'room'          => 'Sala de aula:',
        'artist'        => 'Executante:',
        'finish_till'   => 'É aconselhável terminar até:',
        'tasks_finish'  => 'Tarefas executadas',
        'tasks_over'    => 'Tarefas atrasadas',
        'not'           => 'Não há tarefas do ano passado',
        'manager'       => 'Zelador',
        'begin'         => 'Início',
    ];