<?php

return [
    'sub_edit'      => 'Edição da disciplina',
    'name'          => 'Nome da disciplina',
    'short_name'    => 'Nome abreviado da disciplina',
    'method_del'    => 'Excluir o pacote de materiais didáticos',
];