<?php

return [
    'schedule'  => 'Id do Horário',
    'date'      => 'Data',
    'teach'     => 'Id do Professor',
    'feed'      => 'Aula',
    'class'     => 'Id das Turmas com aulas conjuntas',
    'group'     => 'Id do Grupo',
    'specialty' => 'Id da Especialização',
    'classroom' => 'Id da Sala de aula',
    'active'    => 'Ativo',
];