<?php

    return [
        'report_groups' => 'Report on filling the groups for',
        'report_room'   => 'Report on the occupied classrooms from  _ till _',
        'report_stud'   => 'Report of the use of Mystat by students'
    ];