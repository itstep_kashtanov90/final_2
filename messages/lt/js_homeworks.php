<?php

    return [
    'more_than'     => 'Jūsų pranešimas viršyja simbolių ir bus autimatiškai sumažintas',
    'left'          => 'Liko simbolių:',
    'write'         => 'Parašyti komentarą',
    'are_you_sure'  => 'Ar esate tikras, kad norite panaikinti namų užduotį?',
    'procedure'     => 'Ši procedūra negrįžtamai panaikins Jūsų failą ir visus studentų namų darbus, susijusius su juo!',
    'sel_subject'   => 'Dalyko pasirinkimas',
    're_file'       => 'Ar tikrai norite ištrinti šitą failą?',
    'An_error'      => 'Įvyko klaida, galimas dalykas, kad studento failo serveryje nėra'
];