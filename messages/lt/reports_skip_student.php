<?php

    return [
        'shows'   => 'The report shows the number of classes skipped by the student for the period: day, week, month.',
        'if_stud' => 'If a student missed at least one class for the current month, he/she should be included into the report.',
        'week_7'  => 'A week is considered from the current date minus 7 days, a month is considered as 30 days, depending on the current date, for example, from April 15th to May 15th.',
        'stream'  => 'Class',
        'skipped' => 'Skipped',
        'today'   => 'Today',
        'week'    => 'Week',
        'month'   => 'Month'
    ];