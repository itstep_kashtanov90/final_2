<?php

    return [
        'exam'          => 'Egzaminas',
        'permission_w'  => 'su leidimu',
        'permission_o'  => 'be leidimo',
        'date_end'      => 'Baigimo data',
        'not_specified' => 'nenurodyta',
        'form'          => 'Pažymių įrašymo forma',
        'permission'    => 'Leidimas',
        'mark'          => 'Įvertinimas'
    ];