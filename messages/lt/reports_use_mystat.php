<?php

    return [
        'report'       => 'REPORT ON THE USE OF Mystat BY STUDENTS',
        'city'         => 'City',
        'active_users' => 'Total number of active users',
        'all_time'     => 'Logged in for all the time',
        'all_week'     => 'Logged in for the week',
    ];