<?php

    return [
        'request'           => 'Už prieigos prie rakto prašymas yra metu',
        'date_change'       => 'Pasirinkite datą',
        'message'           => 'Galite siųsti prašymą grįžti į ankstesnį likus ne daugiau nei 3 dienas prieš',
        'cause'             => 'Įveskite priežastį prašymo...',
    ];