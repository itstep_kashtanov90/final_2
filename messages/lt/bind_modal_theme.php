<?php

    return [
        'title'     => 'Savaitės aprašymas',
        'more_than' => 'Savaitės aprašymas negali viršyti 1000 simbolių',
        'left'      => 'Liko simbolių'
    ];