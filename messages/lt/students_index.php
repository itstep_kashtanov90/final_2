<?php

    return [
        'stud'       => 'Students',
        'feedbacks'  => 'Feedbacks about students',
        'feedbacks_'  => 'Feedbacks',
        'mailing'    => 'Mailing',
        'all_groups' => 'All groups',
        'not_groups' => 'You have no group',
        'was'        => 'Visited:',
        'was_s'      => 'Visited:',
        'last_visit' => 'Last visit:',
        'age'        => 'Age:',
        'middle'     => 'Average grade:',
        'attendance' => 'Attendance:',
        'group'      => 'Groups',
        'text'       => 'Text of the feedback',
        'send'       => 'Send',
        'too_short'  => 'The message is too short!',
        'not_stud'   => 'No students',
        'mail_success' => 'Naujienų sėkmingas',
    ];