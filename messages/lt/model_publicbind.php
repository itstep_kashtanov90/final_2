<?php

    return [
        'from_city'    => 'from the city',
        'copy_method'  => 'asks you to copy the methodological package',
        'for_form'     => 'for the educational form',
        'for_feedback' => 'You can use e-mail for feedback',
        'or_phone'     => 'or a phone number',
        'request_copy' => 'Request for copying the methodological package',
        'sel_type'     => 'Select the type',
        'sel_all'      => 'Select all',
        'all_city'     => 'All the cities'
    ];