<?php

    return [
        'month'    => 'Month',
        'week'     => 'Week',
        'day'      => 'Day',
        'pair'     => 'Number of pairs',
        'assigned' => 'assigned',
        'received' => 'received',
        'reviewed' => 'reviewed',
    ];