<?php

    return [
        "theme"         => "Darbo tema",
        "file_teach"    => "Dėstytojo failas",
        "date"          => "Išdavimo data",
        "pair"          => "paskaitų pora",
        "file_stud"     => "Studento failas",
        "date_load"     => "įkėlimo data",
    ];