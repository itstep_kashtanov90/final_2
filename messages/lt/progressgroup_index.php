<?php

    return [
        'sel_group' => 'Selection of the group',
        'sel_sub'   => 'Selection of the subject',
        'homework'  => 'Homework assignment',
        'tests'     => 'Tests',
        'classwork' => 'Classwork',
        'shp'       => 'Absence',
        'late'      => 'Late attendance',
        'comment'   => 'Comment'
    ];