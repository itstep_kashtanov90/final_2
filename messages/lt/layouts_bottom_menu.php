<?php

    return [
        'registration' => 'Time of registration',
        'attendees'    => 'Attendees',
        'timetable'    => 'Timetable',
        'announ'       => 'Announcements',
        'groups'       => 'Attendance groups',
        'materials'    => 'Learning materials',
        'homework'     => 'Homework assignments',
        'classwork'    => 'Classwork',
        'exams'        => 'My exams',
        'reports'      => 'Reports',
        'tasks'        => 'Tasks',
        'stud'         => 'Students',
    ];
