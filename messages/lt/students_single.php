<?php

    return [
        'att'      => 'Attendance',
        'dollars'  => 'Dollars',
        'for_exp'  => 'For excellence in study',
        'crystals' => 'Crystals',
        'for_att'  => 'for the attendance',
        'rewards'  => 'Rewards',
        'single'   => 'Single delivery',
        'perf'     => 'Academic performance:',
        'atts'     => 'Attendance:',
        'spec'     => 'Specialization:',
        'birth'    => 'Date of birth:',
        'mail'     => 'Email of a family member:',
        'networks' => 'Social networks:',
        'leave'    => 'LEAVE A FEEDBACK',
        'phone'    => 'Phone:',
        'address'  => 'Address:',
        'flow'     => 'Flow:',
    ];