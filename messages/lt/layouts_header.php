<?php

    return [
        'profile'     => 'My profile',
        'pass_change' => 'Password change',
        'exit'        => 'Log out',
        'pass_enter'  => 'Enter new password',
        'pass_new'    => 'New password',
        'pass_retype' => 'Retype password',
        'edit'        => 'Edit'
    ];