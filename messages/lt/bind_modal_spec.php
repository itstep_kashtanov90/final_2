<?php

    return [
        'sub_edit'      => 'Redaguoti dalyką',
        'name'          => 'Dalyko pavadinimas',
        'short_name'    => 'Sutrumpintas dalyko pavadinimas',
        'method_del'    => 'Panaikinti metodinės medžiagos paketą'
    ];