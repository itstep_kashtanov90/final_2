<?php

    return [
        're_timetable'      => 'Tvarkaraštis panaikintas.',
        'not_save'          => 'Tvarkaraštis neišsaugotas. Neteisingai nustatyta grupė arba specialybė.',
        'no_stream'         => 'Nėra kursų',
        'no_subject'        => 'Nėra dalykų',
        'no_group'          => 'Nėra grupių',

        'no_room'           => 'Nėra kabinetų',
        'schedule_suc'      => 'Tvarkaraštis sėkmingai dubliuotas',
        'schedule_dup'      => 'Tvarkaraštis dubliuotas su klaidomis',
        'schedule_already'  => 'Šis dėstytojas jau turi tvarkaraštį:',
        'pair'              => 'pamokų pora',

        'rewrite'           => 'Perrašyti?',
        'schedule_save'     => 'Naujas tvarkaraštis išsaugotas',
        'schedule_not_save' => 'Tvarkaraštis neišsaugotas. Jūs nepasirinkote nei vieno kurso arba įvyko klaida.',
        'sel_group twice'   => "Jūs tą pačią grupę pasirinkote du kartus. Prašome panaikinti nereikalingą mygtukuką  arba pasirinkite kitą grupę",
        'group'             => 'Grupė',

        'no_one_sel'        => 'Nepasirinkta nei viena galimybė',
        'date_finich'       => 'Pabaigos data turi būti didesnė, nei pradžios data',
    ];