<?php

    return [
        'work'      => 'Namų darbai',
        'less'      => 'Pamokos',
        'labs'      => 'Laboratoriniai darbai',
        'books'     => 'Knygos',
        'videos'    => 'Vaizdo įrašai',
        'present'   => 'Prezentacijos',
        'tests'     => 'Testai',
        'articles'  => 'Straipsniai',
        'week_add'  => 'Pridėti savaitę'
    ];