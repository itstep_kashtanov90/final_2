<?php

    return [
        'absent'         => 'Absent',
        'passed'         => 'Passed',
        'failed'         => 'Failed',
        'unsatisfactory' => 'Unsatisfactory',
        'satisfactory'   => 'Satisfactory',
        'good'           => 'Good',
        'excellent'      => 'Excellent',
        'denial'         => 'Denial',
        'permission'     => 'Permission'
    ];