<?php

    return [
        'planning'  => 'Schedule planning',
        'remove'    => 'Remove from the schedule',
        'planning'  => 'Schedule planning',
        'Save'      => 'Save this schedule for the whole class',
        'Duplicate' => 'Duplicate the schedule',
        'button'    => 'By means of this button it is possible to save the current state of schedule',
        'remove'    => 'REMOVE FROM THE SCHEDULE',
        'schedule'  => 'schedule clash',
        'inactive'  => 'inactive schedule clash',
    ];