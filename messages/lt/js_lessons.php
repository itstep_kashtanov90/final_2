<?php

    return [
        'no_sel_homework'   => 'Jūs nepasirinkote arba neįkėlėte namų užduoties!',
        'already_dow'       => 'Failas sėkmingai įkeltas',
        'no_sel_laboratory' => 'Jūs nepasirinkote arba neįkėlėte laboratorinio darbo!',
        're_uploaded'       => 'Jūs ruošiatės panaikinti namų užduotį!',
        're_all'            => 'Visos susijusios studentų namų užduotys bus panaikintos.',

        're'                => 'Panaikinti?',
        'theme'             => 'Pamokos tema',
        'no_theme'          => 'Nepavyko išsaugoti temą',
        'at_first'          => 'Iš pradžių būtina nurodyti, ar Jūs esate pagrindinis, ar pakaitinis dėstytojas',
        'in_unlock'         => 'Norėdami atblokuoti, įveskite pamokos temą ir atšaukite radio mygtuką',

        'up_file'           => 'Galima įkelti failą, ne didesnį, nei 100 MB.',
        'should_be_ref'     => 'Čia turi būti nuoroda peržiūrai arba įkėlimui knygos iš išorinio šaltinio, taip pat galite įkelti knygos viršelį',
        'up_order_video'    => 'Teisingam video įrašo įkėlimui būtina įrašyti nuorodą iš adreso eilutės.',
        'up_order_present'  => 'Teisingam prezentacijos įkėlimui būtina įrašyti nuorodą po užrašu "WordPress Shortcode".',
        'cover_ image'      => 'Rekomenduojama naudoti prezentacijų įkėlimo paslaugą: http://www.slideshare.net/',

        'uploading_1'       => 'Po prezentacijos įkėlimo būtina nuspausti mygtuką "Pasidalinti prezentacija".',
        'uploading_2'       => 'ir nukopijuoti kodą iš lauko "WordPress Shortcode".',
        'dow_ref'           => 'Galima įkelto nuorodą į išorinį straipsnį.',
        'cover_ image'      => 'Knygos viršeliu privalo būti paveikslas!',
        'big_size_file'     => 'Serveris negali priimti tokį didelį failą.',

        'apologies'         => 'Nuoširdžiai atsiprašome…',
        'size_ image'       => 'Temos ir aprašymo ilgis turi viršyti 3 simbolius',
        'sel_type'          => 'Pasirinkite tipą',
        'are_you_sure '     => 'Ar Jūs esate įsitikinę, kad norite panaikinti šią medžiagą???',
        'material_not_re'   => 'Medžiaga nepanaikinta',
    ];