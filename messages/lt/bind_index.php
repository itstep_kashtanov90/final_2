<?php

    return [
        "conf"              => "Patvirtinimas",
        "form_all"          => "Visos mokymo formos",
        "form_all_filter"   => "Filtras pagal mokymo formas",
        "form_sel"          => "Pasirinkite mokymo forma",
        "materials"         => "Dėstytojų medžiaga",
        "methods"           => "Metodinė medžiaga",
        "method_add"        => "Paskirti metodinę medžiagą",
        "method_copy"       => "Metodinės medžiagos kopijavimas",
        "method_copy_up"    => "Paprašyti metodinės medžiagos paketo kopijos",
        "new"               => "Nauji",
        "short_name"        => "Sutrumpintas pavadinimas",
        "sub_add"           => "Pridėti dalyką",
        "sub_edit"          => "Redaguoti dalykas",
        "sub_new"           => "Įveskite naują dalyką",
    ];