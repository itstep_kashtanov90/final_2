<?php

    return [
        'method_copy'   => 'Prašymas kopijuoti metodinį paketą',
        'form_sel'      => 'Pasirinkite mokymo formą, kuriai norite sukurti metodinį paketą, kuriam norite sukurti kopiją'
    ];