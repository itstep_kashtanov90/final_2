<?php

    return [
        'attention' => 'Attention!',
        'teach'     => 'Teachers bear personal responsibility for granting a permission to attend classes to students marked as "inadmissible" or unlisted.',
        'stud'      => 'Students having this status may attend classes only after the written permission obtained from a Head Teacher or an Accountant.',
        'confirm'   => 'Confirm'
    ];