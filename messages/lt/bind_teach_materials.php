<?php

    return [
        'type'      => 'Medžiagos tipas',
        'form'      => 'Apmokymų forma',
        'material'  => 'Medžiaga',
        'full_name' => 'Dėstytojo vardas ir pavardė',
        'advise'    => 'Rekomenduoti',
        'no_found'  => 'Pagal nustatytus kriterijus nebuvo rasta nei vieno įrašo',
    ];