<?php

    return [
        'schedule'  => 'Schedule Id',
        'date'      => 'Date',
        'teach'     => "Teacher's Id",
        'feed'      => 'Feed',
        'class'     => 'Class Id',
        'group'     => 'Group Id',
        'specialty' => 'Specialty Id',
        'classroom' => 'Classroom Id',
        'active'    => 'Active'
    ];