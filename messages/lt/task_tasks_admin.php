<?php

    return [
        'data_start'     => 'Starting date',
        'data_end'       => 'Finishing date',
        'control'        => 'Control',
        'not_assigned'   => 'Not assigned',
        'confirm'        => 'Confirm the performance',
        'return_to_work' => 'Return to work',
        'task'           => 'Task',
    ];