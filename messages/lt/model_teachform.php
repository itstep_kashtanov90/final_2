<?php

    return [
        'full_name'  => 'Full name',
        'address'    => 'Address',
        'home_phone' => 'Home phone number',
        'mobile'     => 'Mobile number',
        'Internal'   => 'Internal phone number',
        'corporate'  => 'Corporate Email',
        'birth'      => 'Date of birth',
        'position'   => 'Position',
        'sel_file'   => 'Select the file'
    ];