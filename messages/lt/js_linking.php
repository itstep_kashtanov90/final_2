<?php

    return [
        'sel_subject'   => 'Dalyko pasirinkimas',
        'sel_package'   => 'Paketo pasirinkimas',
        'sel_stream'    => 'Dalyko pasirinkimas',
        'success'       => 'Susiejimas įvykdytas sėkmingai',
        'binding'       => 'Susiejimas jau egzistuoja'
    ];