<?php

    return [
        'sel_file'      => 'Pasirinkite failą',
        'review'        => 'Apžvalga…',
        'select'        => 'Pasirinkite…',
        'no_matches'    => 'Sutapimų nerasta',
        'search'        => 'Paieška…',
    ];