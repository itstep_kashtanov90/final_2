<?php

    return [
        "theme"      => "Subject of the work",
        "file_teach" => "Teacher's file",
        "date"       => "Date of issue",
        "pair"       => "pair",
        "file_stud"  => "File from the student",
        "date_load"  => "Date of loading",
    ];