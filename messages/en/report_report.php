<?php

    return [
        'from'       => 'Period from:',
        'to'         => 'till:',
        'generate'   => 'Generate',
        'hours_past' => 'TAUGHT HOURS',
        'subject'    => 'Subject',
        'hours'      => 'Hours',
        'pairs'      => 'Total number of taught pairs:'
    ];