<?php

    return [
        'report_hours' => 'Report on the taught hours',
        'form_report'  => 'Forms of rendering the reports',
        'report_cons'  => 'Reports on consultations',
        'report_sal'   => 'Reports on salaries and wages'
    ];