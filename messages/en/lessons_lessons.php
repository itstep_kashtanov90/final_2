<?php

    return [
        'teach_main'    => 'Major teacher',
        'request_of_date'    => 'Request of date',
        'material_on' => 'Add material to the group',
        'teach_replace' => 'Substitute teacher',
        'material_add'  => 'Add material',
        'homework'      => 'HOMEWORK ASSIGNMENT',
        'labwork'       => 'LABORATORY WORK',
        'copy_data'     => 'Copy the data from the previous pair',
        'login_mystat'  => 'My stat',
        'cancel_save'   => 'Cancel and save the status "visited" for all the students in the group',
        'mark_all'      => 'Mark all',
        'visit'         => 'Visit',
        'test'          => 'Test',
        'classwork'     => 'Classwork',
        'award'         => 'Awarded',
        'comment'       => 'Comment',
        'to_unlock'     => 'In order to unlock the lesson you need to enter its subject and check the radio button',
        'select'        => 'Select the type',
        'char_left'     => 'Characters left',
        'attention'     => 'Pay attention',
        'theme'         => 'Topic',
        'not_empty'     => 'This field can not be empty',
        'description'   => 'Description',
        'ref_paste'     => 'Paste a reference',
        'file_download' => 'You are not allowed to add a reference (except for the book cover) when the file was downloaded',
        'file_load'     => 'Download the file',
        'ref_if'        => 'If there is a reference, this field is active only for downloading the book cover',
        'button_after'  => 'This button will become active after filling the required fields',
        'add'           => 'Add',
        'home_load'     => 'Download the homework assignment',
        'recommended'   => 'Assignment that is recommended by the teaching department',
        'home_select'   => '---Selection of the homework assignment---',
        'home_exe'      => 'Deadline for the completion of the homework assignments by the students is',
        'review'        => 'Review',
        'theme_less'    => 'Theme of lesson',
        'lab_select'    => '---Selection of laboratory work---',
        'select_type'   => 'Select the type',
    ];