<?php

    return [
        'this_task'   => 'This task has not comments yet',
        'failed_save' => 'Failed to save and upload the comments',
        'del'         => 'delete',
        'View'        => 'View',
        'Edit'        => 'Edit',
        'Confirm'     => 'Confirm execution',
    ];