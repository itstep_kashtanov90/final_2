<?php

    return [
        'task_complet'   => 'The task completion was confirmed',
        'task_sent_back' => 'The assignment was sent back for rework',
        'enter_all'      => 'Please, enter all the necessary data!',
        'not_empty'      => 'It is impossible to add empty comments'
    ];