<?php

    return [
        'exams'   => 'Exams',
        'start'   => 'Start',
        'end'     => 'End',
        'new'     => 'new',
        'group'   => 'Group',
        'subject' => 'Subject',
        'not_found' => 'You do not have the current exams',
    ];