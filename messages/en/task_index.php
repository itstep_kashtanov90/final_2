<?php

    return [
        'tasks_admin'   => 'Tasks for administrators',
        'tasks_manager' => 'Tasks for the assistant manager',
        'tasks_add'     => 'Add a task',
        'complete'      => 'Complete till…',
        'not_rooms'     => 'There are no available classrooms',
        'tasks_create'  => 'Create the task',
        'room'          => 'Classroom:',
        'artist'        => 'Performer:',
        'finish_till'   => 'It is advisable to finish till:',
        'tasks_finish'  => 'Completed tasks',
        'tasks_over'    => 'Overdue tasks',
        'not'           => 'No tasks for the last year',
        'manager'       => 'Manager',
        'begin'         => 'Begin:',
    ];