<?php

    return [
        'colors'    => 'Description of colors',
        'form'      => 'Description of the abbreviation of forms',
        'orange'    => 'orange',
        'not_noted' => 'the teacher has not listed the attendees',
        'red'       => 'red',
        'less_7'    => 'attendance is less than 7 people',
        'lilac'     => 'lilac',
        'less_70'   => 'The level of attendance is less than 70%',
        'green'     => 'green',
        'more_than' => 'The level of attendance is 100% only if more than 7 students are present',
        'p'         => 'P - Professional Computer Education',
        'j'         => 'J - Junior computer Academy',
        'e'         => 'Е - European Computer Education',
        'g'         => 'O - One-year course',
        'classroom' => 'Classroom',
        'pairs'     => 'Pairs'
    ];