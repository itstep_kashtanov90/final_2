<?php

    return [
        'exam'          => 'Exam',
        'permission_w'  => 'with permission',
        'permission_o'  => 'without permission',
        'date_end'      => 'Date of completion',
        'not_specified' => 'not specified',
        'form'          => 'Form of grading',
        'permission'    => 'Permission',
        'mark'          => 'Grade',
        'full_name'          => 'Full name of student',
    ];