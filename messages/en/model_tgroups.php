<?php

    return [
        'not_come'  => 'Had not come',
        'not_phone' => 'Phone number is not specified',
        'not_email' => 'E-mail address is not specified',
        'year'      => 'year',
        'years'     => 'years'
    ];