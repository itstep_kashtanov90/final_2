<?php

    return [
        'not_user'      => 'This user does not exist',
        'mail_confirm'  => 'A mail for confirmation is sent to the e-mail address',
        'pass_change'   => 'The password was changed',
        'mail_not_fond' => 'The user having such an email does not exist',
        'not_city'      => 'You have not selected a city',
        'pass_restore'  => 'Recover password',
        'city'          => 'city',
        'pass'          => 'password',
        'send'          => 'Send',
        'back'          => 'Back'
    ];