<?php

    return [
        'review'    => 'Assignments for review',
        'requests'  => 'Requests for resitting',
        'hometasks' => 'Hometasks',
    ];