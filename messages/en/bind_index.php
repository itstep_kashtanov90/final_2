<?php

    return [
        "conf"            => "Confirmed",
        "form_all"        => "All educational forms",
        "form_all_filter" => "Filter according to the educational forms",
        "form_sel"        => "Select the educational form",
        "materials"       => "Teachers' materials",
        "methods"         => "Methodological materials",
        "method_add"      => "Assign the methodological package",
        "method_copy"     => "Copying the methodological package",
        "method_copy_up"  => "Request a copy of the methodological package",
        "new"             => "New",
        "short_name"      => "Abbreviation",
        "sub_add"         => "Add a subject",
        "sub_edit"        => "Subject editing",
        "sub_new"         => "Enter a new subject",
        "select"          => "Select a subject",
    ];