<?php

    return [
        'more_than'    => 'Your message exceeds the available number of characters and it will be automatically cut.',
        'left'         => 'Characters left:',
        'write'        => 'Write a comment',
        'are_you_sure' => 'Are you sure to remove the homework assignment?',
        'procedure'    => 'By means of this procedure you will permanently delete your file and all the associated home works of students!',
        'sel_subject'  => 'Subject selection',
        're_file'      => 'Are you sure to remove this file?',
        'An_error'     => "An error has occurred, it is possible that the student's file does not exist on the server"
    ];