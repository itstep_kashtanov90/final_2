<?php

    return [
        'not_user'      => 'This user does not exist',
        'mail_confirm'  => 'A mail for confirmation is sent to the e-mail address',
        'pass_changed'  => 'The password was changed',
        'mail_not_fond' => 'The user having such an email does not exist',
        'not_city'      => 'You have not selected a city',
        'login_log'     => 'Login to the electronic log',
        'city'          => 'city',
        'login'         => 'login',
        'pass'          => 'password',
        'enter'         => 'enter',
        'manual'        => 'Logbook system guide',
        're_pass'       => 'Recover password',
    ];