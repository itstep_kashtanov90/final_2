<?php

    return [
        'sel_file'   => 'Select a file',
        'review'     => 'Review…',
        'select'     => 'Select…',
        'no_matches' => 'Matches are not found',
        'search'     => 'Search…',
    ];