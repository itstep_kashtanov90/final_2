<?php

    return [
        'field_login' => 'Fill the Login field',
        'field_pass'  => 'Fill the Password field',
        'login'       => 'Login',
        'pass'        => 'Password',
        'field'       => 'Remember',
        'sel_city'    => 'Select the city'
    ];