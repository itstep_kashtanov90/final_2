<?php

    return [
        'no_laboratory' => 'There are no laboratory works for this group',
        'no_homework'   => 'There are no homework assignments for this group',
        'open_old_student' => 'Display transferred students'
    ];