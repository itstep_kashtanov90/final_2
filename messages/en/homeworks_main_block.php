<?php

    return [
        'open'    => 'Opened',
        'review'  => 'Reviewed',
        'overdue' => 'Overdue',
    ];