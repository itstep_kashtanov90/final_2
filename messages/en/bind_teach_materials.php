<?php

    return [
        'type'        => 'Type of material',
        'form'        => 'Educational form',
        'material'    => 'Material',
        'full_name'   => 'Full name of the teacher',
        'advise'      => 'Advise',
        'no_found'    => 'No records are found according to specified criteria',
        'date'        => 'Date',
        'city'        => 'City',
        'subject'     => 'Subject',
        'theme'       => 'Theme',
        'description' => 'Description',
    ];