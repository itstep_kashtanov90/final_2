<?php

    return [
        'no_sel_homework'   => 'You have not selected or uploaded a homework assignment!',
        'already_dow'       => 'File was successfully uploaded',
        'no_sel_laboratory' => 'You have not selected or uploaded a laboratory work!',
        're_uploaded'       => 'You are going to remove the uploaded homework assignment!',
        're_all'            => 'All the bounded home works received from the students will be removed.',

        're'                => 'Remove?',
        'theme'             => 'Subject of the lesson',
        'no_theme'          => 'Failed to save topic',
        'at_first'          => 'At first, it is necessary to specify if you are a major or a substitute teacher',
        'in_unlock'         => 'In order to perform unlock it is necessary to enter the subject of the lesson and check the radio button',

        'up_file'           => 'It is possible to upload a file that does not exceeds 100 МB.',
        'should_be_ref'     => 'It should be a reference to viewing or downloading a book from the external resource, you may also download a book cover',
        'up_order_video'    => 'In order to upload correct video materials, you need to paste a reference to video from the address bar.',
        'up_order_present'  => 'In order to upload correct presentations, you need to copy a reference available under the inscription "WordPress Shortcode".',
        'cover_ image'      => 'In order to upload the presentations it is advisable to use the following service: http://www.slideshare.net/',

        'uploading_1'       => 'After uploading a presentation it is necessary to press "Share the Presentation" button.',
        'uploading_2'       => 'And copy a code from the WordPress Shortcode field.',
        'dow_ref'           => 'You can download a reference to article from the external resource.',
        'cover_ image'      => 'Book cover should be an image!',
        'big_size_file'     => 'Server is not able to receive the file of such a big size.',

        'apologies'         => 'Please, accept our sincere apologies...',
        'size_ image'       => 'The subject and its description should exceed 3 characters',
        'sel_type'          => 'Select the type',
        'are_you_sure '     => 'Are you sure to remove this material?',
        'material_not_re'   => 'The material was not removed',
    ];