<?php

    return [
        'method_copy' => 'Request for copying the methodological package',
        'form_sel'    => 'Select the educational form for which you are going to create a copy of the methodological package'
    ];