<?php

    return [
        'request'           => 'The request for access to the key is present',
        'date_change'       => 'Choose a date',
        'message'           => 'You can send a request to return to the previous date of no more than 3 days ago',
        'cause'             => 'Enter a reason for the request...',
    ];