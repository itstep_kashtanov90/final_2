<?php

    return [
        'title'     => 'Description of the week',
        'more_than' => 'Description of the week should not exceed 1000 characters',
        'left'      => 'Characters left'
    ];