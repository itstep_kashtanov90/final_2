<?php

    return [
        're_timetable'      => 'The schedule was removed.',
        'not_save'          => 'The schedule was not saved. The group or specialization was incorrectly indicated.',
        'no_stream'         => 'No classes',
        'no_subject'        => 'No subject',
        'no_group'          => 'No groups',

        'no_room'           => 'No classrooms',
        'schedule_suc'      => 'The schedule was successfully duplicated',
        'schedule_dup'      => 'The schedule was duplicated with errors',
        'schedule_already'  => 'The schedule for this teacher already exists:',
        'pair'              => 'pair in',

        'rewrite'           => 'Rewrite?',
        'schedule_save'     => 'New schedule was saved',
        'schedule_not_save' => 'Schedule was not saved. You have not selected any pair or an error has occurred',
        'sel_group twice'   => "You have selected the same group twice. Please, remove unnecessary field with the 'X' button or select another group",
        'group'             => 'Group',

        'no_one_sel'        => 'No one option was selected',
        'date_finich'       => 'The finishing date should be larger that the starting date',
    ];