<?php

    return [
        'all'      => 'ALL',
        'generate' => 'Generate',
        'date'     => 'Date',
        'subject'  => 'Subject',
        'hours'    => 'Hours',
        'total'    => 'Total',
        'not_data' => 'There is no data for this period'
    ];