<?php

    return [
        'teach' => 'Teacher',
        'phone' => 'Phone number',
        'stud'  => 'Student',
        'group' => 'Group',
        'days'  => 'Overdue days'
    ];