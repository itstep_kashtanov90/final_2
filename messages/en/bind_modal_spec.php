<?php

    return [
        'sub_edit'   => 'Subject editing',
        'name'       => 'Subject title',
        'short_name' => 'Abbreviation of the subject',
        'method_del' => 'Remove the methodological package'
    ];