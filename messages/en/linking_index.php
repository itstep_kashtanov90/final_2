<?php

    return [
        'sel_direction' => 'Selection of the educational program',
        'sel_stream'    => 'Selection of the class',
        'sel_subject'   => 'Selection of the subject',
        'sel_package'   => 'Selection of the package',
        'bind'          => 'Bind',
        'are_you_sure'  => 'Are you sure to remove the binding?',
        'remove'        => 'Remove',
    ];