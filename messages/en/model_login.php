<?php

    return [
        're_pass'      => 'Recover the Logbook.itstep.org password',
        'your_pass'    => 'Your password',
        'confirm'      => 'In order to confirm please go to',
        'confirm_pass' => 'Password confirmation!!!'
    ];