<?php

    return [
        'work'     => 'Homework assignments',
        'less'     => 'Lessons',
        'labs'     => 'Laboratory works',
        'books'    => 'Books',
        'videos'   => 'Videos',
        'present'  => 'Presentations',
        'tests'    => 'Tests',
        'articles' => 'Articles',
        'week_add' => 'Add a week'
    ];