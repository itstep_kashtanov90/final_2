<?php

    return [
        'sel_subject' => 'Selection of the subject',
        'sel_package' => 'Selection of the package',
        'sel_stream'  => 'Selection of the class',
        'success'     => 'Binding was successfully performed',
        'binding'     => 'The binding already exists'
    ];