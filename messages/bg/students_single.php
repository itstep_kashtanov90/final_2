﻿<?php

return [
    'att'       => 'Посещение',
    'dollars'   => 'Долари',
    'for_exp'   => 'За успехи в обучението',
    'crystals'  => 'Кристали',
    'for_att'   => 'За посещаемост',
    'rewards'   => 'Награди',
    'single'    => 'Дават се еднократно',
    'perf'      => 'Успеваемост:',
    'atts'      => 'Посещаемост:',
    'spec'      => 'Специализация:',
    'birth'     => 'Дата на раждане:',
    'mail'      => 'Email на родителя:',
    'networks'  => 'Соц. мрежи:',
    'leave'     => 'ОСТАВИ ОТЗИВ',
    'phone'     => 'Телефон:',
    'address'   => 'Адрес:',
    'flow'      => 'Поток:',
];