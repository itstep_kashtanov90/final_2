﻿<?php

return [
    'exam'          => 'Изпит',
    'permission_w'  => 'с допускане',
    'permission_o'  => 'без допускане',
    'date_end'      => 'Дата на приключване',
    'not_specified' => 'не е посочена',
    'form'          => 'Форма на оценяване',
    'permission'    => 'Допускане',
    'mark'          => 'Оценка'
];