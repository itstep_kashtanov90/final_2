﻿<?php

return [
    'from'          => 'Период от:',
    'to'            => 'до:',
    'generate'      => 'Създай',
    'hours_past'    => 'Взети часове',
    'subject'       => 'Предмет',
    'hours'         => 'Часa',
    'pairs'         => 'Общо взети блока:'
];