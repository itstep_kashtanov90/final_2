﻿<?php

return [
    'full_name'     => 'Трите имена',
    'address'       => 'Адрес',
    'home_phone'    => 'Домашен телефон',
    'mobile'        => 'Мобилен телефон',
    'Internal'      => 'Вътрешен телефон',
    'corporate'     => 'Корпоративен Email',
    'birth'         => 'Дата на раждане',
    'position'      => 'Длъжност',
    'sel_file'      => 'Избери файл',
];