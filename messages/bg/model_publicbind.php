﻿<?php

return [
    'from_city'     => 'от град',
    'copy_method'   => 'ви моли за копие на пакета материали',
    'for_form'      => 'За форма на обучение',
    'for_feedback'  => 'За обратна връзка може да използвате e-mail',
    'or_phone'      => 'или телефон',
    'request_copy'  => 'Искане за копиране пакета материали',
    'sel_type'      => 'Избери тип',
    'sel_all'       => 'Избери всичко',
    'all_city'      => 'Всички градове'
];