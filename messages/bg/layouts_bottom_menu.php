﻿<?php

return [
    'stud'          => 'Студенти',
    'registration'  => 'Час на регистрация',
    'attendees'     => 'Присъстващи',
    'timetable'     => 'Разписание',
    'announ'        => 'Съобщения',
    'groups'        => 'Посещаемост на групите',
    'materials'     => 'Учебни материали',
    'homework'      => 'Домашни задания',
    'classwork'     => 'Работа в клас',
    'exams'         => 'Моите изпити',
    'reports'       => 'Отчети',
    'tasks'         => 'Задачи'
];
