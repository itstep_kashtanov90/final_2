﻿<?php

return [
    'schedule'  => 'Id на Разписанието',
    'date'      => 'Дата',
    'teach'     => 'Id на Преподавателя',
    'feed'      => 'Лента',
    'class'     => 'Id на Потока',
    'group'     => 'Id на Групата',
    'specialty' => 'Id на Специалността',
    'classroom' => 'Id на Аудиторията',
    'active'    => 'Активен'
];