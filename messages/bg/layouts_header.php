﻿<?php

return [
    'profile'       => 'Моят профил',
    'pass_change'   => 'Смяна на паролата',
    'exit'          => 'Изход',
    'pass_enter'    => 'Въведи новата парола',
    'pass_new'      => 'Нова парола',
    'pass_retype'   => 'Повтори паролата',
    'edit'          => 'Промени'
];