﻿<?php

    return [
        'exams'     => 'Изпити',
        'start'     => 'Начало',
        'end'       => 'Край',
        'new'       => 'new',
        'group'     => 'Група',
        'subject'   => 'Предмет',
        'not_found' => 'Нямате текущи изпити',
    ];