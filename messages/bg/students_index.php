﻿<?php

    return [
        'stud'       => 'Студенти',
        'feedbacks'  => 'Отзиви за студентите',
        'feedbacks_'  => 'Отзиви',
        'mailing'    => 'Изпращане',
        'all_groups' => 'Всички групи',
        'not_groups' => 'Липсват групи',
        'was'        => 'Явил се:',
        'was_s'      => 'Явила се:',
        'last_visit' => 'Последно посещение:',
        'age'        => 'Възраст:',
        'middle'     => 'Среден бал:',
        'attendance' => 'Посещаемост:',
        'group'      => 'Групи',
        'text'       => 'Текст на отзива',
        'send'       => 'Изпрати',
        'too_short'  => 'Съобщението е прекалено кратко!',
        'not_stud'   => 'Няма студенти',
        'mail_success' => 'Писмо е било успешно',
    ];