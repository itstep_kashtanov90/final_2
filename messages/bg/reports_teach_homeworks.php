﻿<?php

return [
    'month'     => 'Месец',
    'week'      => 'Седмица',
    'day'       => 'Ден',
    'pair'      => 'брой блока',
    'assigned'  => 'зададено',
    'received'  => 'получено',
    'reviewed'  => 'проверено'
];