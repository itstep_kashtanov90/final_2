﻿<?php

return [
    'absent'            => 'Н',
    'passed'            => 'Зачетено',
    'failed'            => 'Незачетено',
    'unsatisfactory'    => 'Неудовлетворително',
    'satisfactory'      => 'Удовлетворително',
    'good'              => 'Добър',
    'excellent'         => 'Отличен',
    'denial'            => 'Не допуснат',
    'permission'        => 'Допуснат'
];