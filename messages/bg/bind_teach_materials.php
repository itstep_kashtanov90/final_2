﻿<?php

    return [
        'type'        => 'Тип материал',
        'form'        => 'Форма на обучениe',
        'material'    => 'Материал',
        'full_name'   => 'Трите имена на преподавателя',
        'advise'      => 'Препоръчано',
        'no_found'    => 'По зададения критерий няма открити резултати',
        'theme'       => 'Тема',
        'subject'     => 'Предмет',
        'description' => 'Описание',
        'date'        => 'Дата',
        'city'        => 'Град',
    ];