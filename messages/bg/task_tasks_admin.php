﻿<?php

    return [
        'data_start'     => 'Дата начало',
        'data_end'       => 'Дата край',
        'control'        => 'Управление',
        'not_assigned'   => 'Не е назначен',
        'confirm'        => 'Подтвърди изпълнението',
        'return_to_work' => 'Върни за довършване',
        'task'           => 'Задание',
    ];