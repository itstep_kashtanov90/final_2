﻿<?php

return [
    'sel_file'      => 'Избери файл',
    'review'        => 'Преглед…',
    'select'        => 'Избери…',
    'no_matches'    => 'Не са открити съвпадения',
    'search'        => 'Търсене…',
];