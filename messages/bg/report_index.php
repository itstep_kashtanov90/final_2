﻿<?php

return [
    'report_hours'  => 'Отчeт за проведените занятия',
    'form_report'   => 'Форми на предаване на отчети',
    'report_cons'   => 'Отчeти за консултаците',
    'report_sal'    => 'Отчeт за ЗП'
];