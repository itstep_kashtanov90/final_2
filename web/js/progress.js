$(document).ajaxStart(function () {
    $(".ajax-loader-overlay").show();
});
$(document).ajaxStop(function () {
    $(".ajax-loader-overlay").hide();
});

/**show photo on mouse enter on picture*/
$(document).on('mouseenter', '.table_all tbody tr td:nth-child(2) .stud_photo', function(){
    console.info("fskhfj");
    //var id_stud=$(this).parent("td").next("td").data('stud');
    //var obj=$(this).parent("td");
    //var url="../students/check-photo";
    //$.ajax({
    //    global: false,//for disable animation on ajaxStart request
    //    url: url,
    //    data: "id_stud="+id_stud,
    //    type:"POST",
    //    success: function(response){
    //        if (response!="none") {
    //            /***get city prefix*/
    //            $.ajax({
    //                global: false,
    //                type: "POST",
    //                url: "exams/get-city-prefix",
    //                success: function(response_city_prefix){
    //                    /***get city id*/
    //                    $('.table_all tbody tr td:nth-child(2)>img').hide();//hide all images
    //                    var path="http://new.logbook.itstep.org/photos/" + response_city_prefix + "/stud/s_" + id_stud + "." + response;
    //                    $('> img', obj).attr('src', path);
    //                    $('> img', obj).show(50);
    //                },
    //                error: function(){}
    //            });
    //        }
    //    },
    //    error: function(){
    //    }
    //});
});

//$(document).on('mouseout', '.table_all tbody tr td:nth-child(2)', function(){
//    $('.table_all tbody tr td:nth-child(2)>img').hide();//hide all images
//});

//$(document).on('click', '.stud_link', function () {
//    var id_stud = $(this).parent("td").data('stud');
//    var group = $('#groups').val();
//    $.cookie('id_stud', id_stud);
//    console.log(id_stud, group);
//    $.ajax({
//        type: "POST",
//        dataType: "html",
//        data: {id_stud: id_stud, group: group, param: 1},
//        url: "../students/single",
//        success: function (response) {
//            $('#progressgroup').removeClass('active');
//            $('#students').addClass('active');
//            $(".style_for_content").html(response);
//        }
//    });
//
//});

$(document).ready(function(){

    /*
    $('#groups').change(function ()
    {
        $(".left_circle").parent().data('curr-table','1');
        $(".table_progress").empty();
        var id_group = $(this).val();
        if(id_group)
        {
            var temp_path =window.location.href.split('/');
            var temp_path =temp_path.pop();
            if(temp_path.length ==0)
                var path = window.location.href + "get-spec-teach/";
            else
                var path = window.location.href + "/get-spec-teach/";

            $.post(path,{id_group: id_group}, function (response) {
                $("#spec").empty();
                $("#spec").append('<option value="">Выбор предмета</option>');
                for (spec in response) {
                    $("#spec").append('<option value="' + response[spec].id_spec + '">' + response[spec].name_spec + '</option>')
                }
                var id_spec = null;
                progress_group(id_group,id_spec);

            }, "json");
        }
        else
        {
            $(".table_progress").empty();
            $("#spec").empty();
            $("#spec").append('<option value="">Выбор потока</option>');
        }
    });
    */

    /////////////////  При загрузке страницы  /////////////////////////////////////////
    //progress_group($('#groups :last').val(),null);
    //$('#groups :last').attr("selected", "selected");
    ////$('#groups').trigger("change");
    //////////////////////////////////////////////////////////////////////////////////

    $('#spec').change(function ()
    { console.log();
        $(".left_circle").parent().data('curr-table','1');
        var id_group = $('#groups').val();
        var id_spec = $(this).val();
        if(id_spec)
        {
            progress_group(id_group,id_spec);
        }
        else
        {
            $(".table_progress").empty();
            progress_group(id_group,id_spec);
        }
    });

    $('#teach').change(function (){

        var id_teach = $(this).val();

        var sel_groups = $("#groups");

        sel_groups.empty();

        if(id_teach != "") {
            $.ajax({
                url: "/base/get-groups-by-teach",
                method: "post",
                data: {
                    id_teach: id_teach
                },
                //dataType: "json",
                context: document.body,
                success: function (response) {

                    var data = JSON.parse(response);

                    sel_groups.append('<option value="">Выбор группы</option>');

                    $.each(data, function (i, v) {
                        sel_groups.append('<option value="' + v.id_tgroups + '">' + v.name_tgroups + '</option>')
                    })

                }
            });
        }
    });

    $('#groups').change(function (){

        var id_tgroups = $(this).val();

        var sel_groups = $("#stud");

        sel_groups.empty();

        if(id_tgroups != "") {
            $.ajax({
                url: "/base/get-stud-by-group",
                method: "post",
                data: {
                    id_tgroups: id_tgroups
                },
                //dataType: "json",
                context: document.body,
                success: function (response) {

                    var data = JSON.parse(response);

                    sel_groups.append('<option value="">Выбор студента</option>');

                    $.each(data, function (i, v) {
                        sel_groups.append('<option value="' + v.id_stud + '">' + v.fio_stud + '</option>')
                    })

                }
            });
        }
    });

    $('#type').change(function (){

        var type = $(this).val();

        var file = $("#file");

        console.log(type);

        if(type != '') {

            $.ajax({
                url: "/base/for-type",
                method: "post",
                data: {
                    type: type
                },
                context: document.body,
                success: function (response) {
                    file.html(response);
                }
            });

        } else {
            file.html('');
        }
    });

    function progress_group(id_group,id_spec)
    {
        //console.log(id_group, id_spec);
        var temp_path =window.location.href.split('/');
        var temp_path =temp_path.pop();
        //console.log(temp_path);
        if(temp_path.length ==0)
            var path = window.location.href + "get-list-group/";
        else
            var path = window.location.href + "/get-list-group/";

        $.post(path,{id_group: id_group,id_spec:id_spec}, function (response) {
            $(".table_progress").html(response);
        }, "html");
    }


    $('.right_circle').click(function(e){
        e.preventDefault();
        var curr_table = parseInt($(this).parent().data('curr-table'),10);
        var last_table = $("table.pg_table").last();
        var last = last_table.data('table');
         last = parseInt(last,10);
        if(curr_table < last)
        {
            var now = curr_table + 1;
            $(this).parent().data('curr-table',now);
            $(".pg_table[data-table='"+curr_table+"']").hide();
            $(".pg_table[data-table='"+now+"']").show();

        }

    });

    $('.left_circle').click(function(e){
        e.preventDefault();
        var curr_table = parseInt($(this).parent().data('curr-table'),10);
        if(curr_table > 1)
        {
            var now = curr_table - 1;
            $(this).parent().data('curr-table',now);
            $(".pg_table[data-table='"+curr_table+"']").hide();
            $(".pg_table[data-table='"+now+"']").show();

        }

    });

    $("[data-this_active='true']").on("click", function(){
        console.log($(this).data("this_active"));

        if($(this).data("this_active") == true) {
            var elem = {};

            elem.id_digest = $(this).data("digest-id");
            elem.name = $(this).data("digest-el");
            elem.text = $(this).text().trim();
            elem.control = $(this);

            var input = '<div class="col-md-6">';
            input += '<input type="text" name="' + elem.name + '" value="' + elem.text.trim() + '">';
            input += '</div>';

                if(elem.name == "comment"){
                    input = '<div class="col-md-5">';
                    input += '<textarea name="' + elem.name + '">' + elem.text.trim() + '</textarea>';
                    input += '</div>';
                } else if(elem.name == "mark"){
                    input = '<div class="col-md-3">';
                    input += '<select name="mark" class="form-control font_opensans select_form3">';
                    input += '</div>';
                    for(i = 0; i <= 12; i++){
                        if(elem.text == i){
                            input += '<option value="' + i + '" selected>' + i + '</option>'
                        } else {
                            input += '<option value="' + i + '">' + i + '</option>'
                        }
                    }
                    input += '</select>';
                    input += '</div>';
                }
            var buttons = '<i class="glyphicon glyphicon-ok" aria-hidden="true" data-digest-edit="' + elem.name + '"></i>';
            buttons += '<i class="glyphicon glyphicon-remove" aria-hidden="true" data-digest-cancel="' + elem.name + '"></i>';

            var html = elem.control.html();

            elem.control.html('<div class="row">' + input + '<div class="col-md-1">' + buttons + '</div></div>');
            elem.control.data("this_active", "wait");

        } else if($(this).data("this_active") == 'false') {
            $(this).data("this_active", true);
        }

        $("[data-digest-edit]").on("click", function(){

            var input_val = $("[name='" + elem.name + "']");
            input_val = $(input_val[0]).val();
            input_val.trim();

            $.ajax({
                url: "/portfolio/edit-digest",
                method: "post",
                data: {
                    id_digest: elem.id_digest,
                    elem: elem.name,
                    val: input_val
                },
                //dataType: "json",
                context: document.body,
                success: function (response) {

                    elem.control.text(input_val);
                    elem.control.append('<i class="glyphicon glyphicon-pencil" aria-hidden="true"></i>');
                    elem.control.data("this_active", "false").end();

                    console.info(response);
                }
            });
        });

        $("[data-digest-cancel]").on("click", function(){

            console.clear();
            elem.control.html(html);
            elem.control.data("this_active", "false").end();

        });

        $("[data-scroll]").on("click", function(){
            console.log('sdfsdf');
            //document.getElementById('showScroll').innerHTML = 10 + 'px';
        });
    });
    $( "#target" ).on("submit", function( event ) {
        if(!confirm("Вы действительно хотите удалить Дайджест!")){
            return false;
        }
    });

    $( "[data-material-delete]" ).on("click", function( event ) {

        var id_digest = $("#id_digest").val();
        var id_material = $(this).data("material-delete");
        var link = window.location.href;

        if(confirm("Вы действительно хотите удалить Материал!")){

            $.ajax({
                url: "/portfolio/delete-material",
                method: "post",
                data: {
                    id_digest: id_digest,
                    id_material: id_material
                },
                success: function (response) {
                    $("[data-block-material-id='" + id_material + "']").remove();
                }
            });

        }
    });

    $("[data-material-this_active='true']").on("click", function() {
        console.log($(this).data("material-this_active"));

        if ($(this).data("material-this_active") == true) {
            var elem = {};

            elem.id_material = $(this).data("material-id");
            elem.name = $(this).data("material-el");
            elem.text = $(this).text().trim();
            elem.control = $(this);

            input = '<div class="col-md-7">';
            input += '<textarea name="' + elem.name + '">' + elem.text.trim() + '</textarea>';
            input += '</div>';

            var buttons = '<i class="glyphicon glyphicon-ok" aria-hidden="true" data-material-edit="' + elem.name + '"></i>';
            buttons += '<i class="glyphicon glyphicon-remove" aria-hidden="true" data-material-cancel="' + elem.name + '"></i>';

            var html = elem.control.html();

            elem.control.html('<div class="row">' + input + '<div class="col-md-1">' + buttons + '</div></div>');
            elem.control.data("material-this_active", "wait");

        } else if ($(this).data("material-this_active") == 'false') {
            $(this).data("material-this_active", true);
        }

        $("[data-material-edit]").on("click", function () {

            console.log('start');

            var input_val = $("[name='" + elem.name + "']");
            input_val = $(input_val).val();
            input_val.trim();

            console.log(input_val, elem.name, elem.id_material);

            $.ajax({
                url: "/portfolio/edit-material",
                method: "post",
                data: {
                    id_material: elem.id_material,
                    comment: input_val
                },
                //dataType: "json",
                context: document.body,
                success: function (response) {

                    elem.control.text(input_val);
                    elem.control.append('<i class="glyphicon glyphicon-pencil" aria-hidden="true"></i>');
                    console.log('save');
            //        //elem.control.append('<i class="glyphicon glyphicon-pencil" aria-hidden="true"></i>');
                    elem.control.data("material-this_active", "false").end();

                    console.info(response);
                },
                errors: function (response) {
                    console.info(response);
                }
            });
        });

        $("[data-material-cancel]").on("click", function () {

            console.clear();
            elem.control.html(html);
            elem.control.data("material-this_active", "false").end();

        });
    });
});

var file_name = '';

function CheckForm_isFilled_path_or_saveFile(form){

    var input_path = $("[name='DigestMaterials[path]']").val();
    var input_file = file_name;

    if(input_path != '' || file_name != ''){
        //form.submit();
        return true;
    } else {
        //$("#error_for_input_path_or_saveFile").html('');
        return false;
    }

}

function tstFile(val){
    file_name = val.value;
}
